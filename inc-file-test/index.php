<?php 

#include myClass 
include_once("testclass.inc");
#include function
include_once("testfunction.inc");

#instantiate myClass
$obj = new myClass();	
$obj->print_text("text");
echo "</br>";

#instantiate using overriding methods
myClass::print_text("text");
echo "</br>";

#prints the sum of the sum function
echo sum(1, 1);
?>