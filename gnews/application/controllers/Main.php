<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function insert_data(){
        $data = $this->input->post();
        $this->Article_model->insert_data($data);
        redirect(base_url("main/complaints_suggestion/1"));
    }

    public function complaints_suggestion($message = null){
        $data['title'] = "Complaints And Suggestion";
        $data['title'] = "Government News Home";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/home';
        $config["total_rows"] = $this->Article_model->article_count();
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->Article_model->get_article($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data["message"] = $message;
        $this->load->view('main/header2', $data);
        $this->load->view('main/complaints', $data);
        $this->load->view('main/footer', $data);
    }

    function index() {
        redirect('main/home', 'refresh');
    }

    function home() {
        $data['title'] = "Government News Home";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/home';
        $config["total_rows"] = $this->Article_model->article_count();
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->Article_model->get_article($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();


        $this->load->view('main/header2', $data);
        $this->load->view('main/home', $data);
        $this->load->view('main/footer', $data);
    }

    /* GET ARTICLE BY CATEGORY */

    function article() {
        $catid = $this->input->get('catid');

        $data['title'] = "Government News Articles";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/article?catid=' . $catid;
        $config["total_rows"] = $this->Article_model->article_category_count($catid);
        $config["per_page"] = 5;
        /* $config["uri_segment"] = 4;| */
        $config["query_string_segment"] = 'page_no';
        $config["page_query_string"] = TRUE;

        $this->pagination->initialize($config);

        /* if ($this->uri->segment(4)) {
          $page = ($this->uri->segment(4));
          } else {
          $page = 0;
          } */
        $page = ($this->input->get('page_no') != NULL ? $this->input->get('page_no') : 0);
        $data["results"] = $this->Article_model->get_category_article($config["per_page"], $page, $catid);
        $data["links"] = $this->pagination->create_links();


        $this->load->view('main/header', $data);
        $this->load->view('main/article', $data);
        $this->load->view('main/footer', $data);
    }

    /* END GET ARTICLE BY CATEGORY */

    /* VIEW ARTICLE DETAILS */

    function view_article($id) {
        $data = array('title' => 'View Article');
        //$data['article_cat'] = $this->get_acat();
        $data['cat_count'] = $this->Article_model->get_acat_count();
        $this->set_article_viewcount($id);
        $data['article_details'] = $this->get_article_details($id);

        $this->load->view('main/header', $data);
        $this->load->view('main/view_article', $data);
        $this->load->view('main/footer', $data);
    }

    function get_article_details($id) {
        $query = $this->db->query("SELECT tbl_article.ArticleID, tbl_article.Title, tbl_article.Content, tbl_article.Author,tbl_article.ViewCounter, tbl_article.Date_posted, tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID WHERE tbl_article.ArticleID = " . $id . ";");
        $data[] = $query->row();
        return $data;
    }

    function set_article_viewcount($id) {
        $this->db->query("UPDATE tbl_article SET ViewCounter = ViewCounter + 1 WHERE ArticleID = ?;", array($id));
    }

    function get_article_comment_count($id) {
        $query = $this->db->query("SELECT * FROM tbl_comments WHERE ArticleID = " . $id . ";");
        $row = $query->num_rows();
        return $row;
    }

    function get_article_comment($id) {
        $query = $this->db->query("SELECT * FROM ");
    }

    /* END VIEW ARTICLE DETAILS */

    /* START LOGIN PAGE */

    function login() {
        $data = array('title' => 'Government News Login');
        $data['cat_count'] = $this->Article_model->get_acat_count();
        $data['reg_message'] = '';
        $this->load->view('main/header', $data);
        $this->load->view('main/login', $data);
        $this->load->view('main/footer', $data);
    }

    function logout() {
        unset($_SESSION['front_user']);
        redirect('main', 'refresh');
    }


    function facebook_login(){
       // Load facebook library and pass associative array which contains appId and secret key
    require APPPATH.'libraries\facebook\src\facebook.php';
    // Get user's login information
       $facebook = new Facebook(array(
      'appId'  => '704762989699153',
      'secret' => 'e5df8bc8ce146094e43f7f299d17d49e',
    ));
        
       $user = $facebook->getUser();
       if ($user) {
              try {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me');
                echo "login";
              } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
                print_r(error_log($e));
              }
        } else {
            echo "not working";
        }
    }


    function userweblogin() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $query = $this->db->query("SELECT * FROM tbl_front_user WHERE username = ?;", array($username));
        $row = $query->row();
        if (isset($row)) {//CHECK IF USERNAME EXIST
            $dbpass = $row->password;

            if ($password === $dbpass) {//CHECK IF PASSWORD MATCH
                $fbdata = array('fname' => $row->fname, 'lname' => $row->lname, 'email' => $row->email, 'userid' => $row->UserID, 'front_user' => '1');
                $this->session->set_userdata($fbdata);
                redirect('main', 'refresh');
            } else {//PASSWORD NOT MATCH
                $message = '<div class = "alert alert-warning" role = "alert">';
                $message .= '<strong>Sorry</strong> Username or Password Incorrect.';
                $message .= '</div>';

                unset($_POST);

                $data = array('title' => 'Government News Login');
                $data['cat_count'] = $this->Article_model->get_acat_count();
                $data['reg_message'] = $message;
                $this->load->view('main/header', $data);
                $this->load->view('main/login', $data);
                $this->load->view('main/footer', $data);
            }
        } else { //USERNAME NOT FOUND
            $message = '<div class = "alert alert-warning" role = "alert">';
            $message .= '<strong>Sorry</strong> Username or Password Incorrect.';
            $message .= '</div>';

            unset($_POST);

            $data = array('title' => 'Government News Login');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $data['reg_message'] = $message;
            $this->load->view('main/header', $data);
            $this->load->view('main/login', $data);
            $this->load->view('main/footer', $data);
        }
    }

    function userfblogin() {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $fbid = $this->input->post('id');
        $fbpicture = $this->input->post('picture');




        $query = $this->db->query("SELECT * FROM tbl_front_user WHERE fb_id = '" . $fbid . "'");
        $row = $query->row();
        if (isset($row)) { /* REGISTERED FB LOGIN */
            $this->db->query("UPDATE `gnews_db`.`tbl_front_user` SET `fb_fname` = ?, `fb_lname` = ?, `fb_picture`= ? WHERE fb_id = ?;", array($fname, $lname, $fbpicture, $fbid));
            $fbdata = array('fbid' => $fbid, 'fname' => $fname, 'lname' => $lname, 'email' => $email);
            $this->session->set_userdata($fbdata);
            echo 'REGISTERED';
        } else { /* NOT YET REGISTERED */
            $this->db->query("INSERT INTO `gnews_db`.`tbl_front_user` (`fname`, `lname`, `email`, `fb_id`, `fb_fname`,`fb_lname`,`fb_picture`) VALUES (?, ?, ?, ?,?,?,?) ; ", array($fname, $lname, $email, $fbid, $fname, $lname, $fbpicture));
            $fbdata = array('fbid' => $fbid, 'fname' => $fname, 'lname' => $lname, 'email' => $email);
            $this->session->set_userdata($fbdata);
            echo 'REGISTERED';
        }
    }

    function get_fblogin_userdata() {
        $query = $this->db->query("SELECT * FROM tbl_front_user WHERE fb_id = '" . $this->session->userdata('fbid') . "'");
        $row = $query->row();
        $fbdata = array('fbid' => $row->fb_id, 'fname' => $row->fname, 'lname' => $row->lname, 'email' => $row->email, 'userid' => $row->UserID, 'front_user' => '1');

        $this->session->set_userdata($fbdata);
        redirect('main', 'refresh');
    }

    /* END LOGIN PAGE */

    /* START REGISTER PAGE */

    function register() {

        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[tbl_front_user.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('conpass', 'Confirm Password', 'required|min_length[8]|matches[password]');
        $this->form_validation->set_rules('answer', 'Security Answer', 'required');
        $this->form_validation->set_rules('reg_question', 'Security Question', 'required|xss_clean');
        $this->form_validation->set_message('is_unique', 'The {field} is not available.');


        if ($this->form_validation->run() == FALSE) {
            $data = array('title' => 'Government News Register');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $data['reg_message'] = '';
            $this->load->view('main/header', $data);
            $this->load->view('main/register', $data);
            $this->load->view('main/footer', $data);
        } else {
            $p = $this->Article_model->get_generated_pass();
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $question = $this->input->post('reg_question');
            $answer = $this->input->post('answer');
            $username = $this->input->post('username');

            $this->db->query("INSERT INTO `gnews_db`.`tbl_front_user` (`fname`, `lname`, `email`, `username`, `password`, `QuestionID`, `QuestionAnswer` ) VALUES (?, ?, ?, ?, ?, ? ,?) ; ", array($fname, $lname, $email, $username, $pass, $question, $answer));


            $data = array('title' => 'Government News Register');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $message = '<div class = "alert alert-success" role = "alert">';
            $message .= '<strong>Thank You!</strong> Successfully registered to Government News Blog.';
            $message .= '</div>';

            unset($_POST);
            $data['reg_message'] = $message;
            $this->load->view('main/header', $data);
            $this->load->view('main/register', $data);
            $this->load->view('main/footer', $data);

            /* $message = "Good Day, Please follow this link " . base_url('main/activate?v=') . $p . "&i=" . $this->db->insert_id() . " to activate you account. Thank You - Government News";

              $config = Array(
              'protocol' => 'smtp',
              'smtp_host' => 'ssl://smtp.mail.yahoo.com',
              'smtp_port' => 465,
              'smtl_crypto' => 'ssl',
              'smtp_user' => 'selegna_8@yahoo.com.ph', // change it to yours
              'smtp_pass' => 'akosiaaronneil', // change it to yours
              'mailtype' => 'text',
              'charset' => 'iso-8859-1',
              'wordwrap' => TRUE
              );
              $this->load->library('email', $config);

              $this->email->set_newline("\r\n");

              $this->email->from('governmentnews-reply.com', 'Government News');
              $this->email->to($email);

              $this->email->subject('Account Activation');
              $this->email->message($message);

              $this->email->send();

              echo $message; */
        }
    }

    /* END REGISTER PAGE */

    /* START SEARCH ARTICLE */

    function search_article() {
        $searchby = $this->input->post('search_by');
        $searchfor = $this->input->post('search_for');
        $data['title'] = "Government News Search Article";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/search_article';
        $config["total_rows"] = $this->Article_model->search_article_count($searchby, $searchfor);
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->Article_model->get_search_article($config["per_page"], $page, $searchby, $searchfor);
        $data["links"] = $this->pagination->create_links();
        $data["total_row"] = $this->Article_model->search_article_count($searchby, $searchfor);

        $this->load->view('main/header', $data);
        $this->load->view('main/search', $data);
        $this->load->view('main/footer', $data);
    }

    /* END SEARCH ARTICLE */

    /* START FREEDOM WALL */

    function freedomwall() {
        $data['title'] = "Government News Freedom Wall";
        $data['cat_count'] = $this->Article_model->get_acat_count();
        $this->load->view('main/header', $data);
        $this->load->view('main/freedomwall', $data);
        $this->load->view('main/footer', $data);
    }

    /* END FREEDOM WALL */

    /* StART PROFILE */

    function profile() {
        $data['title'] = "Government News User Profile";
        $data['reg_message'] = '';
        $data['cat_count'] = $this->Article_model->get_acat_count();
        $data['userdata'] = $this->get_userdata($this->session->userdata('userid'));

        $this->load->view('main/header', $data);
        $this->load->view('main/profile', $data);
        $this->load->view('main/footer', $data);
    }

    function get_userdata($userid) {
        $query = $this->db->query("SELECT * FROM tbl_front_user WHERE UserID = ?;", array($userid));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function update_userdetails() {
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');



        if ($this->form_validation->run() == FALSE) {
            $data = array('title' => 'Government News User Profile');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $data['reg_message'] = '';
            $data['userdata'] = $this->get_userdata($this->session->userdata('userid'));
            $this->load->view('main/header', $data);
            $this->load->view('main/profile', $data);
            $this->load->view('main/footer', $data);
        } else {
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $email = $this->input->post('email');
            $userid = $this->session->userdata('userid');

            $this->db->query("UPDATE tbl_front_user SET fname = ?, lname = ?, email = ? WHERE UserID = ?;", array($fname, $lname, $email, $userid));


            $data = array('title' => 'Government News User Profile');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $data['userdata'] = $this->get_userdata($this->session->userdata('userid'));
            $message = '<div class = "alert alert-success" role = "alert">';
            $message .= 'Successfully updated User Details.';
            $message .= '</div>';

            $data['reg_message'] = $message;
            $this->load->view('main/header', $data);
            $this->load->view('main/profile', $data);
            $this->load->view('main/footer', $data);
        }
    }

    function update_account_credentials() {
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[tbl_front_user.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('conpass', 'Confirm Password', 'required|min_length[8]|matches[password]');
        $this->form_validation->set_rules('answer', 'Security Answer', 'required');
        $this->form_validation->set_rules('reg_question', 'Security Question', 'required|xss_clean');
        $this->form_validation->set_message('is_unique', 'The {field} is not available.');


        if ($this->form_validation->run() == FALSE) {
            $data = array('title' => 'Government News User Profile');
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $data['reg_message'] = '';
            $data['userdata'] = $this->get_userdata($this->session->userdata('userid'));
            $this->load->view('main/header', $data);
            $this->load->view('main/profile', $data);
            $this->load->view('main/footer', $data);
        } else {
            $pass = $this->input->post('password');
            $question = $this->input->post('reg_question');
            $answer = $this->input->post('answer');
            $username = $this->input->post('username');
            $userid = $this->session->userdata('userid');

            $this->db->query("UPDATE tbl_front_user SET username = ?, password = ?, QuestionID = ?, QuestionAnswer = ? WHERE UserID = ?;", array($username, $pass, $question, $answer, $userid));

            $data = array('title' => 'Government News User Profile');
            $data['userdata'] = $this->get_userdata($this->session->userdata('userid'));
            $data['cat_count'] = $this->Article_model->get_acat_count();
            $message = '<div class = "alert alert-success" role = "alert">';
            $message .= 'Successfully updated Account Credentials.';
            $message .= '</div>';

            unset($_POST);
            $data['reg_message'] = $message;
            $this->load->view('main/header', $data);
            $this->load->view('main/profile', $data);
            $this->load->view('main/footer', $data);
        }
        /* END PROFILE */
    }

    function removefb() {
        $userid = $this->input->post('userdata');

        $this->db->query("UPDATE tbl_front_user SET fb_id = ?,fb_fname = ?,fb_lname=?,fb_picture=? WHERE UserID = ?;", array(NULL, NULL, NULL, NULL, $userid));
        echo 'DONE';
    }

    function userlinkfb() {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $fbid = $this->input->post('id');
        $fbpicture = $this->input->post('picture');
        $userid = $this->session->userdata('userid');
        $this->db->query("UPDATE `gnews_db`.`tbl_front_user` SET `fb_fname` = ?, `fb_lname` = ?, `fb_picture`= ? , fb_id = ? WHERE UserID = ?;", array($fname, $lname, $fbpicture, $fbid, $userid));

        echo 'REGISTERED';
    }

    function forgotpassword() {
        $data = array('title' => 'Government News User Profile');
        $data['cat_count'] = $this->Article_model->get_acat_count();
        $data['reg_message'] = '';
        $this->load->view('main/header', $data);
        $this->load->view('main/forgotpassword', $data);
        $this->load->view('main/footer', $data);
    }

      function latest_updates() {
        $data['title'] = "Latest Updates";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/latest_updates';
        $config["total_rows"] = $this->Article_model->article_count2();
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->Article_model->get_article2($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();


        $this->load->view('main/header2', $data);
        $this->load->view('main/home2', $data);
        $this->load->view('main/footer2', $data);
    }


     function search_article2() {
        if(!$this->input->post('search_for')) redirect(base_url('main/latest_updates'));
        $search = $this->input->post('search_for');
        $data['title'] = "Government News Search Article";
        $data['cat_count'] = $this->Article_model->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul class='pagination pagination-lg'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url() . 'main/search_article2';
        $config["total_rows"] = $this->Article_model->search_article_count2($search);
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = $this->Article_model->search_article_count2($search);
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->Article_model->get_search_article2($config["per_page"], $page, $search);
        $data["links"] = $this->pagination->create_links();
        $data["total_row"] = $this->Article_model->search_article_count2($search);
        $this->load->view('main/header2', $data);
        $this->load->view('main/search2', $data);
        $this->load->view('main/footer2', $data);
    }

}
