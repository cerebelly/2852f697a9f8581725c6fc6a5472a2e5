<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userbackend extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }
        
        function index()
        {
            if ($this->session->userdata('username') == TRUE)//CHECK IF THERE IS LOGGED IN ACCOUNT
            {
                redirect('userbackend/article_management', 'refresh');
            }
            else//NO LOG IN
            {
                redirect('userbackend/login', 'refresh');
            }
            
        }
        
        function login()
        {
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>','</div>');
            if ($this->form_validation->run() == FALSE)
                {
                        $data['login_error'] = '';
                        $this->load->view('userbackend/login',$data);
                }
                else
                {
                    $username = $this->input->post('username');
                    $password = do_hash($this->input->post('password'));
                    
                    $query = $this->db->query("SELECT tbl_users.UserID, tbl_users.`Password`, CONCAT( tbl_users.Fname, ' ', tbl_users.Lname ) AS `Name`, tbl_users.StatusID, tbl_users.TypeID FROM tbl_users WHERE tbl_users.Username = '".$username."';");
                    $row = $query->row();
                    
                    if(isset($row))//CHECK IF HAS ROWS
                    {
                        $dbpassword = $row->Password;
                        $dbname = $row->Name;
                        $status = $row->StatusID;
                        $type = $row->TypeID;
                        $userid = $row->UserID;
                        
                        if ($type  >= '1')//CHECK IF USER OR ADMIN
                        {
                            
                            if ($status === '1')//CHECK IF ACTIVE ACCOUNT
                            {
                                
                                if ($password === $dbpassword)//CHECK IF PASSWORD MATCH
                                {
                                    $sesdata = array('username' => $dbname, 'name' => $dbname, 'userid' => $userid);
                                    $this->session->set_userdata($sesdata);
                                    redirect('userbackend', 'refresh');
                                }
                                else//PASSWORD DOESN'T MATCH
                                {
                                    $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Wrong Username or Password.</div>';
                                    $this->load->view('userbackend/login',$data);
                                }
                                
                            }
                            else//NOT ACTIVE
                            {
                                $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Sorry your account is currently inactive.</div>';
                                $this->load->view('userbackend/login',$data);
                            }
                            
                        }
                        else//NOT ADMIN
                        {
                            $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Sorry you don\'t have enough privileges to use this function.</div>';
                            $this->load->view('userbackend/login',$data);
                        }
                        
                    }
                    else//USERNAME NOT FOUND
                    {
                        $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Wrong Username or Password.</div>';
                        $this->load->view('userbackend/login',$data);
                        
                    }
                        
                }
        }
                
                function article_management()
                {
                    if ($this->session->userdata('username') == false)
            {
                redirect('userbackend','refresh');
            }
            else
            {
                $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_article')
            ->set_subject('Article');
            $crud->columns('ArticleID','Title','Content','Author','Date_posted','CatID');
            $crud->display_as('ArticleID', 'Article ID');
            $crud->display_as('CatID', 'Article Category');
            $crud->display_as('Date_posted', 'Date Posted');
            $crud->set_relation('CatID', 'tbl_article_category', 'Name');
            $crud->where('UserID',$this->session->userdata('userid'));
            
            $crud->field_type('UserID', 'hidden', $this->session->userdata('userid'));
            $crud->field_type('Author', 'hidden', $this->session->userdata('username'));
            $crud->field_type('Date_posted','hidden',  date('Y-m-d'));
            $output = $crud->render();
 
            $this->load->view('userbackend/header',$output);
            $this->load->view('userbackend/articlemanagement',$output);
            $this->load->view('userbackend/footer',$output);
            }
                }
                
         function logout()
         {
             $this->session->unset_userdata('username');
             $this->session->unset_userdata('name');
             redirect('userbackend','refresh');
         }
}
        
        
