<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function index() {
        redirect('gnews/home','refresh');
    }
    
    function home()
    {   
        
        $data = array('title' => 'Home');
        //$data['article_cat'] = $this->get_acat();
        $data['cat_count'] = $this->get_acat_count();

        $config = array();
        $config['full_tag_open'] = "<ul>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config["base_url"] = base_url().'gnews/home';
        $config["total_rows"] = $this->article_count();
        $config["use_page_numbers"] = TRUE;
        $config["per_page"] = 3;
        $config["uri_segment"] = 3;
        $config["page_query_string"] = FALSE;
        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 0;
        }
        $data["results"] = $this->get_article($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();


        $this->load->view('frontend/header2', $data);
        $this->load->view('frontend/home2', $data);
        $this->load->view('frontend/footer2', $data);
    }

  /*  function article_count() {
        $query = $this->db->query("SELECT * FROM tbl_article;");
        $row = $query->num_rows();
        return $row;
    }*/

     function article_count() {
        $query = $this->db->query("SELECT * FROM contents;");
        $row = $query->num_rows();
        return $row;
    }

    function get_article($limit, $start) {
        $query = $this->db->query("SELECT tbl_article.ArticleID, tbl_article.Title, tbl_article.Content, tbl_article.Author, tbl_article.ViewCounter, tbl_article.Date_posted, tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID ORDER BY ArticleID DESC LIMIT " . $limit . " OFFSET " . $start . ";");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    

    function get_acat_count() {
        /*$data = $this->get_acat();
        foreach ($data as $rows) {
            $query = $this->db->query("SELECT Count(tbl_article.CatID) AS RowCount, tbl_article_category.`Name`, tbl_article.CatID FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID WHERE tbl_article_category.`Name` = '" . $rows->Name . "'");
            $rc = $query->row();
            $rcount[] = $rc;
        }
        return $rcount;*/
    }
    
    function view_article($id)
    {
        $data = array('title' => 'View Article');
        //$data['article_cat'] = $this->get_acat();
        $data['cat_count'] = $this->get_acat_count();
        $this->set_article_viewcount($id);
        $data['article_details'] = $this->get_article_details($id);
        
        $this->load->view('frontend/header2', $data);
        $this->load->view('frontend/view_article', $data);
        $this->load->view('frontend/footer2', $data);
    }
    
    function get_article_details($id)
    {
        $query = $this->db->query("SELECT tbl_article.ArticleID, tbl_article.Title, tbl_article.Content, tbl_article.Author,tbl_article.ViewCounter, tbl_article.Date_posted, tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID WHERE tbl_article.ArticleID = ".$id.";");
        $data[] = $query->row();
        return $data;
    }
    
    function set_article_viewcount($id)
    {
        $this->db->query("UPDATE tbl_article SET ViewCounter = ViewCounter + 1 WHERE ArticleID = ?;",array($id));
    }
    

}
