<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminbackend extends CI_Controller {

        public function __construct()
        {
                parent::__construct();

                // Your own constructor code
        }
        
        function index()
        {
            if ($this->session->userdata('name') == TRUE)//CHECK IF THERE IS LOGGED IN ACCOUNT
            {
                redirect('adminbackend/usermanagement', 'refresh');
            }
            else//NO LOG IN
            {
                redirect('adminbackend/login', 'refresh');
            }
            
        }
        
        function login()
        {
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>','</div>');
            if ($this->form_validation->run() == FALSE)
                {
                        $data['login_error'] = '';
                        $this->load->view('adminbackend/login',$data);
                }
                else
                {
                    $username = $this->input->post('username');
                    $password = do_hash($this->input->post('password'));
                    
                    $query = $this->db->query("SELECT tbl_users.`Password`, CONCAT( tbl_users.Fname, ' ', tbl_users.Lname ) AS `Name`, tbl_users.StatusID, tbl_users.TypeID FROM tbl_users WHERE tbl_users.Username = '".$username."';");
                    $row = $query->row();
                    
                    if(isset($row))//CHECK IF HAS ROWS
                    {
                        $dbpassword = $row->Password;
                        $dbname = $row->Name;
                        $status = $row->StatusID;
                        $type = $row->TypeID;
                        
                        if ($type  === '1')//CHECK IF ADMIN
                        {
                            
                            if ($status === '1')//CHECK IF ACTIVE ACCOUNT
                            {
                                
                                if(true)//if ($password === $dbpassword)//CHECK IF PASSWORD MATCH
                                {
                                    $this->session->set_userdata('name',$dbname);
                                    $this->session->set_userdata('username',$dbname);
                                    redirect('adminbackend', 'refresh');
                                }
                                else//PASSWORD DOESN'T MATCH
                                {
                                    $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Wrong Username or Password.</div>';
                                    $this->load->view('adminbackend/login',$data);
                                }
                                
                            }
                            else//NOT ACTIVE
                            {
                                $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Sorry your account is currently inactive.</div>';
                                $this->load->view('adminbackend/login',$data);
                            }
                            
                        }
                        else//NOT ADMIN
                        {
                            $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Sorry you don\'t have enough privileges to use this function.</div>';
                            $this->load->view('adminbackend/login',$data);
                        }
                        
                    }
                    else//USERNAME NOT FOUND
                    {
                        $data['login_error'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>Wrong Username or Password.</div>';
                        $this->load->view('adminbackend/login',$data);
                        
                    }
                        
                }
        }
        
        function logout()
        {
            $this->session->unset_userdata('name');
            redirect('adminbackend', 'refresh');
            
        }
        
        function usermanagement()
        {
            if ($this->session->userdata('name') == false)
            {
                redirect('adminbackend');
            }
            else
            {
            $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_users')
            ->set_subject('Users');
            $crud->columns('UserID','Username','Fname','Mname','Lname','Department','Email','StatusID','TypeID');
            $crud->display_as('UserID','User ID');
            $crud->display_as('Fname', 'First Name');
            $crud->display_as('Mname', 'Middle Name');
            $crud->display_as('Lname', 'Last Name');
            $crud->display_as('StatusID', 'Account Status');
            $crud->display_as('TypeID', 'Account Privilege');
            $crud->set_relation('StatusID', 'tbl_status', 'Description');
            $crud->set_relation('TypeID','tbl_type','Description');
            
            
            $crud->unset_add_fields('StatusID');
            $crud->set_rules('Username', 'Username', 'required');
            $crud->set_rules('Password','Password','required|min_length[8]');
            $crud->set_rules('Fname','First Name','required');
            $crud->set_rules('Mname','Middle Name','required');
            $crud->set_rules('Lname','Last Name','required');
            $crud->set_rules('Department','Department','required');
            $crud->set_rules('TypeID','Account Privilege','required');
            $crud->set_rules('Email','Email','required|valid_email');
            $crud->set_rules('StatusID','Account Status','required');
            
            $crud->callback_field('Password',array($this,'set_password_empty'));
            $crud->callback_update(array($this,'encrypt_password_callback'));
            
            $output = $crud->render();
 
            $this->load->view('adminbackend/header',$output);
            $this->load->view('adminbackend/usermanagement',$output);
            $this->load->view('adminbackend/footer',$output);
            }
        }
        function encrypt_password_callback($post_array,$primary_key)
        {
                if(!empty($post_array['Password']))
                {
                    $post_array['Password'] = do_hash($post_array['Password']);
                }
                else
                {
                    unset($post_array['Password']);
                }
                return $this->db->update('tbl_users',$post_array,array('UserID'=>$primary_key));
        }       
        
        function set_password_empty($value)
        {
            return "<input type='password' name='Password' value='' placeholder='password' />";
        }
        
        function articlecategory()
        {
            if ($this->session->userdata('name') == false)
            {
                redirect('adminbackend');
            }
            else
            {
                $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_article_category')
            ->set_subject('Article Categories');
            $crud->columns('CatID','Name','StatusID');
            $crud->display_as('CatID','Category ID');
            $crud->display_as('Name', 'Category Name');
            $crud->display_as('StatusID', 'Category Status');
            $crud->set_relation('StatusID', 'tbl_status', 'Description');
            
            
            $crud->unset_add_fields('StatusID');
            $crud->set_rules('Name', 'Category Name', 'required');
            $crud->set_rules('StatusID','Category Status','required');
            
            $output = $crud->render();
 
            $this->load->view('adminbackend/header',$output);
            $this->load->view('adminbackend/articlecat',$output);
            $this->load->view('adminbackend/footer',$output);
            }
            
        }


        function update_crawlers($success=null,$data = null)
        {
            
            if ($this->session->userdata('name') == false)
            {
                redirect('adminbackend');
            }
            else
            {
                
                $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_article_category')
            ->set_subject('Article Categories');
            $crud->columns('CatID','Name','StatusID');
            $crud->display_as('CatID','Category ID');
            $crud->display_as('Name', 'Category Name');
            $crud->display_as('StatusID', 'Category Status');
            $crud->set_relation('StatusID', 'tbl_status', 'Description');
            
            
            $crud->unset_add_fields('StatusID');
            $crud->set_rules('Name', 'Category Name', 'required');
            $crud->set_rules('StatusID','Category Status','required');
            
           

            $output = $crud->render();
                
             $this->load->view('adminbackend/header',$output);
            $this->load->view('adminbackend/crawler', $output);
            $this->load->view('adminbackend/footer',$output);
            }
            
        }

          function view_complaints_and_suggestions()
        {
            
            
            
                
                $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_article_category')
            ->set_subject('Article Categories');
            $crud->columns('CatID','Name','StatusID');
            $crud->display_as('CatID','Category ID');
            $crud->display_as('Name', 'Category Name');
            $crud->display_as('StatusID', 'Category Status');
            $crud->set_relation('StatusID', 'tbl_status', 'Description');
            
            
            $crud->unset_add_fields('StatusID');
            $crud->set_rules('Name', 'Category Name', 'required');
            $crud->set_rules('StatusID','Category Status','required');
            
           

            $output = $crud->render();
              $this->db->order_by("id");
                $result = $this->db->get("complaints_suggestion");
            $data =  $result->result_array();
            $this->session->set_userdata("tables",$data);
            $output = $crud->render();
                
             $this->load->view('adminbackend/header',$output);
            $this->load->view('adminbackend/view_complaint', $output);
            $this->load->view('adminbackend/footer',$output);
      }

      function view_complaints($id){

          $crud = new grocery_CRUD();
 
            $crud->set_table('tbl_article_category')
            ->set_subject('Article Categories');
            $crud->columns('CatID','Name','StatusID');
            $crud->display_as('CatID','Category ID');
            $crud->display_as('Name', 'Category Name');
            $crud->display_as('StatusID', 'Category Status');
            $crud->set_relation('StatusID', 'tbl_status', 'Description');
            
            
            $crud->unset_add_fields('StatusID');
            $crud->set_rules('Name', 'Category Name', 'required');
            $crud->set_rules('StatusID','Category Status','required');
            
           

            $output = $crud->render();
              $this->db->order_by("id");
              $this->db->where("id", $id);
                $result = $this->db->get("complaints_suggestion");
            $data =  $result->result_array();
            $datas = end($data);
            $this->session->set_userdata("tables",$datas);
            $output = $crud->render();
                
             $this->load->view('adminbackend/header',$output);
            $this->load->view('adminbackend/view_complaint_by_id', $output);
            $this->load->view('adminbackend/footer',$output);

      }
            
       

}

