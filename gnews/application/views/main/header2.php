
<!DOCTYPE HTML>
<html>

    <!-- Mirrored from p.w3layouts.com/demos/aug-2016/02-08-2016/style_blog/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Aug 2016 17:51:53 GMT -->
    <head>
        <?php
        $this->output->set_header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Pragma: no-cache");
        ?>
        <title><?php echo $title; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Style Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url('assets/main/css/bootstrap.css'); ?>" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="<?php echo base_url('assets/main/css/raleway.css'); ?>" rel='stylesheet' type='text/css' />	
        <link href="<?php echo base_url('assets/main/css/style.css'); ?>" rel='stylesheet' type='text/css' />	
        <link href="<?php echo base_url('assets/main/css/chat.css'); ?>" rel='stylesheet' type='text/css' />	
        <script src="<?php echo base_url('assets/main/js/jquery-1.11.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/main/js/bootstrap.min.js'); ?>"></script>
        <!-- animation-effect -->
        <link href="<?php echo base_url('assets/main/css/animate.min.css'); ?>" rel="stylesheet"> 
        <script src="<?php echo base_url('assets/main/js/wow.min.js'); ?>"></script>
        
        
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
    </head>
    <body>

        <div class="header" id="ban">
            <div class="container">
                <div class="header_right wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                            <nav class="link-effect-7" id="link-effect-7">
                                <ul class="nav navbar-nav">
                                    <li class="active act"><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li><a href="<?php echo base_url('main/latest_updates');?>">News</a></li>
             
                                    <li><a href="about.html">About</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="<?php echo base_url('main/complaints_suggestion'); ?>">Complaints & Suggestion</a></li>
                                    
                                </ul>
                            </nav>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
                <div class="nav navbar-nav navbar-right  fadeInRight animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        $CI = & get_instance();
                        $front_user = $CI->session->userdata('front_user');
                        $user_name= $CI->session->userdata('fname').' '.$CI->session->userdata('lname');

                        if ($front_user == FALSE) {
                            echo '<li><a href="' . base_url('main/login') . '">LOGIN</a></li>';
                            echo '<li><a href="' . base_url('main/register') . '">REGISTER</a></li>';
                        } else {
                            echo '<li><a href="'.base_url('main/freedomwall').'">FREEDOM WALL</a></li>';
                            echo '<li><a href="'.base_url('main/profile').'">'.$user_name.'</a></li>';
                            echo '<li><a href="'.base_url('main/logout').'">LOG OUT</a></li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="clearfix"> </div>	
            </div>
        </div>
        <!--start-main-->
        <div class="header-bottom">
            <div class="container">
                <div class="logo wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
                    <h1><a href="index-2.html">GOVERNMENT NEWS</a></h1>
                    <p><label class="of"></label>INFORMATION FOR EVERYJUAN<label class="on"></label></p>
                </div>
            </div>
        </div>

        <div class="technology">
            <div class="container">