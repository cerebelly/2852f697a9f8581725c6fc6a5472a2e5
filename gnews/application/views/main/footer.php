<!-- technology-right -->
<div class="col-md-3 technology-right">


    <div class="blo-top1">

        <div class="tech-btm">
            <div class="insta wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>Search Article </h4>
            <form method="post" action="<?php echo base_url('main/search_article');?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Search By</label>
                    <select class="form-control" name="search_by">
                        <option value="Title">Article Title</option>
                        <option value="Author">Article Author</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Search For</label>
                    <input type="text" class="form-control" name="search_for" id="exampleInputPassword1" placeholder="Search For" required>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-1 btn-primary">Search</button>
                    </div>
                </div>

            </form>
                
            <div class="clearfix"> </div>
            </div>
            <br>
            <h4>Article Categories </h4>

            <div class="blog-grids wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
                
                <div class="blog-grid-right">
                    <?php
                    foreach ($cat_count as $cat) {
                        echo '<h5><a href="' . base_url('main/article?catid=' . $cat->CatID) . '"><span class="category-name">' . $cat->Name . '</span> <span class="count">(' . $cat->RowCount . ')</span></a> </h5>';
                    }
                    ?>

                </div>
                <div class="clearfix"> </div>
            </div>
            <h4>MMDA Twiiter</h4>
             <div> 
                 <a class="twitter-timeline" href="https://twitter.com/MMDA">Tweets by MMDA</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
             </div>   
            <div class="clearfix"> </div>
            </div>
            <br>
            

            
        </div>



    </div>


</div>
<div class="clearfix"></div>
<!-- technology-right -->
</div>
</div>
<div class="footer">
    <!--<div class="container">
        <div class="col-md-4 footer-left wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>About Me</h4>
            <p>Consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod .</p>
            <img src="images/t4.jpg" class="img-responsive" alt="">
            <div class="bht1">
                <a href="singlepage.html">Read More</a>
            </div>
        </div>
        <div class="col-md-4 footer-middle wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>Latest Tweet</h4>
            <div class="mid-btm">
                <p>Sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod .</p>
                <a href="https://w3layouts.com/">https://w3layouts.com/</a>
            </div>

            <p>Consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod .</p>
            <a href="https://w3layouts.com/">https://w3layouts.com/</a>

        </div>
        <div class="col-md-4 footer-right wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>Newsletter</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
            <div class="name">
                <form action="#" method="post">
                    <input type="text" placeholder="Your Name" required="">
                    <input type="text" placeholder="Your Email" required="">
                    <input type="submit" value="Subscribed Now">
                </form>

            </div>	

            <div class="clearfix"> </div>

        </div>
        <div class="clearfix"></div>
    </div>-->
</div>
<div class="copyright wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
    <div class="container">
        <p> | © 2016 Government News. All rights reserved | </p>
    </div>
</div>
</body>

<!-- Mirrored from p.w3layouts.com/demos/aug-2016/02-08-2016/style_blog/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Aug 2016 17:51:53 GMT -->
</html>