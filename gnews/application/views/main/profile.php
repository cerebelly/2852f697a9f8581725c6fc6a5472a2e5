<script  type="text/javascript">
    function remove_fb()
    {
        var userid = <?php echo $this->session->userdata('userid'); ?>;
        $.post("<?php echo base_url('main/removefb'); ?>", {userdata: userid}, function (data) {
            console.log(data);
            if (data === "DONE")
            {
                window.location.assign("<?php echo base_url('main/profile'); ?>");
            }
            //alert(data);
        });
    }

</script>
<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
            console.log(JSON.stringify(response));
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '1155962427807632',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.5' // use graph api version 2.5
        });

        // Now that we've initialized the JavaScript SDK, we call 
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        //FB.getLoginStatus(function (response) {
        //    statusChangeCallback(response);
        // });

    };

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=first_name, last_name, email,name,picture,id', function (response) {
            console.log('Successful login for: ' + response.first_name);

            console.log(JSON.stringify(response));

            $.post("<?php echo base_url('main/userlinkfb'); ?>", {fname: response.first_name, lname: response.last_name, email: response.email, id: response.id, picture: JSON.stringify(response.picture)}, function (data) {
                console.log(data);
                if (data === "REGISTERED")
                {
                    window.location.assign("<?php echo base_url('main/profile'); ?>");
                }
                //alert(data);
            });

        });

        FB.logout(function (response) {
            console.log('LOG OUT');
        });
    }
</script>


<div class="col-md-9 technology-left">
    <div class="tech-no">


        <h2 class="w3">PROFILE</h2>
        <div class="contact-grids">
            <div class="col-md-12">
                <?php
                foreach ($userdata as $row) {
                    $fname = $row->fname;
                    $lname = $row->lname;
                    $email = $row->email;
                    $picture = json_decode($row->fb_picture, true);

                    $username = $row->username;
                    $password = $row->password;
                    $questionid = $row->QuestionID;
                    $questionanswer = $row->QuestionAnswer;

                    $fb_id = $row->fb_id;
                    $fbpicture = $picture['data'];
                    $fb_fname = $row->fb_fname;
                    $fb_lname = $row->fb_lname;
                }
                ?>

                <?php echo $reg_message; ?>

                <div class="row animated wow fadeInRight" data-wow-duration="1000ms">
                    <div class="col-sm-12">
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <h3 class="panel-title">USER DETAILS</h3>
                            </div>
                            <div class="panel-body" style="height: auto;">
                                <form  method="post" action="<?php echo base_url('main/update_userdetails'); ?>">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">First Name* <span class="text-danger"><?php echo form_error('fname'); ?></span></label>
                                        <input type="text" class="form-control" value="<?php
                                        $CI = & get_instance();
                                        if ($CI->form_validation->run() == FALSE) {
                                            echo $fname;
                                        } else {
                                            echo set_value('fname');
                                        }
                                        ?>"  placeholder="First Name" name="fname">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name* <span class="text-danger"><?php echo form_error('lname'); ?></span></label>
                                        <input type="text" class="form-control" value="<?php
                                        $CI = & get_instance();
                                        if ($CI->form_validation->run() == FALSE) {
                                            echo $lname;
                                        } else {
                                            echo set_value('lname');
                                        }
                                        ?>"  placeholder="Last Name" name="lname">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email Address* <span class="text-danger"><?php echo form_error('email'); ?></span></label>
                                        <input type="text" class="form-control" value="<?php
                                        $CI = & get_instance();
                                        if ($CI->form_validation->run() == FALSE) {
                                            echo $email;
                                        } else {
                                            echo set_value('email');
                                        }
                                        ?>"  placeholder="Email" name="email">
                                    </div>


                                    <button style="width: 200px;"  type="submit" class="btn btn-1 btn-success">UPDATE</button>
                                </form>
                                <br>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ACCOUNT CREDENTIALS</h3>
                            </div>
                            <div class="panel-body" style="height: auto;">
                                <?php
                                if ($username == FALSE) {
                                    $message = '<div class = "alert alert-warning" role = "alert">';
                                    $message .= 'Setup Account Credentials to allow login with Username.';
                                    $message .= '</div>';
                                    echo $message;
                                }
                                ?>

                                <form  method="post" action="<?php echo base_url('main/update_account_credentials'); ?>">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Username* <?php
                                            if ($username == FALSE) {
                                                echo '<span class="text-danger">' . form_error('username') . '</span>';
                                            } else {
                                                echo '';
                                            }
                                            ?></label>
                                        <input type="text" class="form-control" value="<?php
                                        $CI = & get_instance();
                                        if ($CI->form_validation->run() == FALSE) {
                                            echo $username;
                                        } else {
                                            echo set_value('username');
                                        }
                                        ?>" 
                                               <?php
                                               if ($username == FALSE) {
                                                   echo 'name="username"';
                                               } else {
                                                   echo 'disabled';
                                               }
                                               ?>  placeholder="Username" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password* <span class="text-danger"><?php echo form_error('password'); ?></span></label>
                                        <input type="password" class="form-control"  placeholder="Password" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Confirm Password* <span class="text-danger"><?php echo form_error('conpass'); ?></span></label>
                                        <input type="password" class="form-control"  placeholder="Password"  name="conpass">
                                    </div>

                                    <div class="form-group ">
                                        <label>Security Question <span class="text-danger"><?php echo form_error('reg_question'); ?></span></label>
                                        <select class="form-control" name="reg_question">
                                            <option value=""  <?php echo set_select('reg_question', '', TRUE) ?>>Select Question</option>
                                            <option value="0"  
                                            <?php
                                            $CI = & get_instance();
                                            if ($CI->form_validation->run() == FALSE) {
                                                if ($questionid === "0") {
                                                    echo 'selected';
                                                }
                                            } else {
                                                echo set_select("reg_question", "0");
                                            }
                                            ?>>What was the name of your elementary / primary school?</option>

                                            <option value="1" <?php
                                            $CI = & get_instance();
                                            if ($CI->form_validation->run() == FALSE) {
                                                if ($questionid === "1") {
                                                    echo 'selected';
                                                }
                                            } else {
                                                echo set_select("reg_question", "1");
                                            }
                                            ?>>What is your pet’s name?</option>

                                            <option value="2" <?php
                                            $CI = & get_instance();
                                            if ($CI->form_validation->run() == FALSE) {
                                                if ($questionid === "2") {
                                                    echo 'selected';
                                                }
                                            } else {
                                                echo set_select("reg_question", "2");
                                            }
                                            ?>>What is your favorite movie?</option>

                                            <option value="3" <?php
                                            $CI = & get_instance();
                                            if ($CI->form_validation->run() == FALSE) {
                                                if ($questionid === "3") {
                                                    echo 'selected';
                                                }
                                            } else {
                                                echo set_select("reg_question", "3");
                                            }
                                            ?>>What is your favorite color?</option>
                                        </select>


                                    </div>

                                    <div class="form-group login_input">
                                        <label>Security Answer <span class="text-danger"><?php echo form_error('answer'); ?></span></label>
                                        <input type="text" class="form-control" value="<?php
                                        $CI = & get_instance();
                                        if ($CI->form_validation->run() == FALSE) {
                                            echo $questionanswer;
                                        } else {
                                            echo set_value('answer');
                                        }
                                        ?>"  placeholder="Enter Security Answer" name="answer">

                                    </div>

                                    <button style="width: 200px;"  type="submit" class="btn btn-1 btn-success">UPDATE</button>
                                </form>
                                <br>
                            </div>
                        </div>
                    </div><!-- /.col-sm-4 -->
                    <div class="col-sm-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">FACEBOOK LOGIN</h3>
                            </div>
                            <div class="panel-body" style="height: auto;">
                                <?php
                                if ($fb_id == FALSE) {
                                    echo '<h4> <span class="label label-primary">Link Facebook Account</span></h4><br>';
                                    echo '<div class="fb-login-button" data-max-rows="1" data-size="xlarge" data-show-faces="false" data-scope="public_profile,email" onLogin="checkLoginState()" data-auto-logout-link="false"></div>';
                                } else {
                                    echo '<div class="media">';
                                        echo '<div class="media-left media-middle">';
                                        echo '<a href="#">';
                                        echo '<img class="media-object" src="' . $fbpicture['url'] . '" alt="'. $fb_fname .' '. $fb_lname .'">';
                                        echo '</a>';
                                        echo '</div>';

                                        echo '<div class = "media-body">';
                                            echo '<h4 class = "media-heading">' . $fb_fname . ' ' . $fb_lname . '</h4>';
                                            echo '<h4> <span class="label label-primary">Linked with Facebook</span></h4>';
                                        echo '</div>';
                                    echo '</div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div><!-- /.col-sm-4 -->

                </div>


            </div>
        </div>

    </div>
</div>



