<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
            console.log(JSON.stringify(response));
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '1155962427807632',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.5' // use graph api version 2.5
        });

        // Now that we've initialized the JavaScript SDK, we call 
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        //FB.getLoginStatus(function (response) {
        //    statusChangeCallback(response);
       // });

    };

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=first_name, last_name, email,name,picture,id', function (response) {
            console.log('Successful login for: ' + response.first_name);

            console.log(JSON.stringify(response));

            $.post("<?php echo base_url('main/userfblogin'); ?>", {fname: response.first_name, lname: response.last_name, email: response.email, id: response.id,picture: JSON.stringify(response.picture)}, function (data) {
                console.log(data);
                if (data === "REGISTERED")
                {
                    window.location.assign("<?php echo base_url('main/get_fblogin_userdata');?>");
                }
                //alert(data);
            });

        });

        FB.logout(function (response) {
            console.log('LOG OUT');
        });
    }
</script>


<div class="col-md-9 technology-left">
    <div class="tech-no">


        <h2 class="w3">LOGIN</h2>
        <div class="contact-grids">
            <div class="col-md-8">
                <?php echo $reg_message;?>

                <form method="post" action="<?php echo base_url('main/userweblogin');?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required>
                    </div>

                    <button style="width: 200px;"  type="submit" class="btn btn-1 btn-primary">LOGIN</button>
                    <a style="width: 200px;" class="btn btn-1 btn-info" href="<?php echo base_url('main/forgotpassword'); ?>" role="button">FORGOT PASSWORD</a>
                </form>
                <br>

            </div>
            <div class="col-md-4">
                <h4>Or Login With</h4>
                <div class="contact-top">


                    <div class="clearfix"></div>
                </div>
                <div class="fb-login-button" data-max-rows="1" data-size="xlarge" data-show-faces="false" data-scope="public_profile,email" onLogin="checkLoginState()" data-auto-logout-link="false"></div>
                
                <!--<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
                </fb:login-button>-->
            </div>
            <div class="clearfix"></div>
        </div>


    </div>
</div>



