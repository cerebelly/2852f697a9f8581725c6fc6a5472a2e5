<div class="col-md-9 technology-left">
    <div class="tech-no">


        <h2 class="w3">REGISTER</h2>
        <div class="contact-grids">
            <div class="col-md-8">
                <?php echo $reg_message;?>

                <form  method="post" action="<?php echo base_url('main/register'); ?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name* <span class="text-danger"><?php echo form_error('fname'); ?></span></label>
                        <input type="text" class="form-control" value="<?php echo set_value('fname'); ?>"  placeholder="First Name" name="fname">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name* <span class="text-danger"><?php echo form_error('lname'); ?></span></label>
                        <input type="text" class="form-control" value="<?php echo set_value('lname'); ?>"  placeholder="Last Name" name="lname">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Address* <span class="text-danger"><?php echo form_error('email'); ?></span></label>
                        <input type="text" class="form-control" value="<?php echo set_value('email'); ?>"  placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username* <span class="text-danger"><?php echo form_error('username'); ?></span></label>
                        <input type="text" class="form-control" value="<?php echo set_value('username'); ?>"  placeholder="Username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password* <span class="text-danger"><?php echo form_error('password'); ?></span></label>
                        <input type="password" class="form-control"  placeholder="Password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Confirm Password* <span class="text-danger"><?php echo form_error('conpass'); ?></span></label>
                        <input type="password" class="form-control"  placeholder="Password"  name="conpass">
                    </div>

                    <div class="form-group ">
                        <label>Security Question <span class="text-danger"><?php echo form_error('reg_question'); ?></span></label>
                        <select class="form-control" name="reg_question">
                            <option value="" <?php echo set_select('reg_question', '', TRUE) ?>>Select Question</option>
                            <option value="0"  <?php echo set_select("reg_question", "0"); ?>>What was the name of your elementary / primary school?</option>
                            <option value="1" <?php echo set_select("reg_question", "1"); ?>>What is your pet’s name?</option>
                            <option value="2" <?php echo set_select("reg_question", "2"); ?>>What is your favorite movie?</option>
                            <option value="3" <?php echo set_select("reg_question", "3"); ?>>What is your favorite color?</option>
                        </select>
                        

                    </div>

                    <div class="form-group login_input">
                        <label>Security Answer <span class="text-danger"><?php echo form_error('answer'); ?></span></label>
                        <input type="text" class="form-control" value="<?php echo set_value('answer'); ?>"  placeholder="Enter Security Answer" name="answer">

                    </div>

                    <button style="width: 200px;"  type="submit" class="btn btn-1 btn-success">REGISTER</button>
                </form>
            </div>
        </div>

    </div>
</div>



