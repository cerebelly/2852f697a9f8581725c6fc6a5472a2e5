
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="col-md-9 technology-left">
    <div class="agileinfo">

        <div class="single">
            <?php
            foreach ($article_details as $row) {
                $title = $row->Title;
                $content = $row->Content;
                $author = $row->Author;
                $article_date = strtotime($row->Date_posted);
                $article_type = $row->Name;
                $article_viewcount = $row->ViewCounter;
                $string = preg_replace('~<p>(.*?)</p>~is', '$1', $content, /* limit */ 1);
            }
            ?>
            <div class="b-bottom"> 
                <h5 class="top"><?php echo $title; ?></h5>
                <p class="sub"><?php echo $string; ?></p>

                <p>By <?php echo $author;?> On <?php echo date('F j, Y', $article_date); ?> | Category: <?php echo $article_type;?><span class="glyphicon glyphicon-eye-open"></span><?php echo $article_viewcount; ?></p>

            </div>
        </div>


        <div class="response">
            <h4>Responses</h4>
            <div class="fb-comments" data-href="<?php echo current_url();?>" data-numposts="10"   data-mobile="false"></div>
        </div>	
        <div class="clearfix"></div>
    </div>
</div>