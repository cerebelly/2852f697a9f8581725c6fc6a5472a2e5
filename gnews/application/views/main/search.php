<div class="col-md-9 technology-left">
    <div class="tech-no">
        <!-- technology-top -->

        <?php
        if ($results != FALSE) {
            echo '<div class = "alert alert-success" role = "alert">';
            echo '<strong> '.$total_row.'</strong> Search Results.';
            echo '</div>';
            foreach ($results as $row) {
                $article_id = $row->ArticleID;
                $article_title = $row->Title;
                $article_content = $row->Content;
                $article_author = $row->Author;
                $article_date = strtotime($row->Date_posted);
                $article_type = $row->Name;
                $article_viewcount = $row->ViewCounter;


                $view = '<div class="tc-ch wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">';
                $view .= '<h3><a href="' . base_url('main/view_article/' . $article_id . '') . '">' . $article_title . '</a></h3>';
                $view .= '<h6>BY <a href="singlepage.html">' . $article_author . ' </a>' . date('F j, Y', $article_date) . '.</h6>';
                $view .= '<p>' . character_limiter(strip_tags($article_content), 20) . '</p>';
                $view .= '<div class="bht1">';
                $view .= '<a href="' . base_url('main/view_article/' . $article_id . '') . '">Continue Reading</a>';
                $view .= '</div>';
                $view .= '<div class="clearfix"></div>';
                $view .= '</div>';
                $view .= '<div class="clearfix"></div>';



                /* $view = '<h3><a href="' . base_url('gnews/view_article/' . $article_id) . '">' . $article_title . '</a></h3>';
                  $view .= '<span>By ' . $article_author . ' - ' . date('F j, Y', $article_date) . ' | Article Type: ' . $article_type . ' | View Count: ' . $article_viewcount . '</span> ';
                  $view .= '<div class="data">';
                  $view .= '<p>' . character_limiter(strip_tags($article_content), 20) . '</p>';
                  $view .= '<span><a href="' . base_url('gnews/view_article/' . $article_id) . '">Continue reading >>></a></span>';
                  $view .= '</div><div class="clear"></div>'; */
                echo $view;
            }
        } else {
            echo '<div class = "alert alert-warning" role = "alert">';
            echo '<strong>We\'re Sorry</strong> '.$total_row.' Search Results.<br>Please try again.';
            echo '</div>';
        }
        ?>
        <!-- 
         <div class="tc-ch wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
 
 
             <h3><a href="singlepage.html">Lorem Ipsum is simply</a></h3>
             <h6>BY <a href="singlepage.html">ADAM ROSE </a>JULY 10 2016.</h6>
             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud eiusmod tempor incididunt ut labore et dolore magna aliqua exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
             <p>Ut enim ad minim veniam, quis nostrud eiusmod tempor incididunt ut labore et dolore magna aliqua exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
             <div class="bht1">
                 <a href="singlepage.html">Continue Reading</a>
             </div>
             <div class="clearfix"></div>
         </div>
         <div class="clearfix"></div>-->
        <!-- technology-top -->
        <nav class="paging">
            <?php
            echo $links;
            ?>
            <!--<ul class="pagination pagination-lg">
                    <li><a href="#" aria-label="Previous"><span aria-hidden="true"><<</span></a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#" aria-label="Next"><span aria-hidden="true">>></span></a></li>
            </ul>-->
        </nav>
    </div>
</div>

