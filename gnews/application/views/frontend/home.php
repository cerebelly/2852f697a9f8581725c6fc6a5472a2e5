<!--------------------- Main Content ------------------->
<div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-scope="public_profile,email" onLogin="checkLoginState()" data-auto-logout-link="true"></div>

<div id="status"></div>
<div class="wrap">
    <div class="main">
        <div class="content">
            <div class="box1">

                <form class="form-inline" method="post" action="<?php echo current_url() . '?segment=inbox&q=search&page_no=0' ?>">
                    <div class="form-group">
                        <label for="category">Search By</label>
                        <select class="form-control" name="category" placeholder="">
                            <option value="" disabled select selected>-Select Category-</option> 
                            <option value="Title">Title</option>
                            <option value="Content">Content</option>
                            <option value="Author">Author</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="field"> Search For</label>
                        <input type="text" name="field" class="form-control" placeholder="Input Search">
                    </div>
                    <button type="submit" class="btn btn-warning" name="search">Search</button>
                </form>
                <hr>    
                <?php
                foreach ($results as $row) {
                    $article_id = $row->ArticleID;
                    $article_title = $row->Title;
                    $article_content = $row->Content;
                    $article_author = $row->Author;
                    $article_date = strtotime($row->Date_posted);
                    $article_type = $row->Name;
                    $article_viewcount = $row->ViewCounter;

                    $view = '<h3><a href="' . base_url('gnews/view_article/' . $article_id) . '">' . $article_title . '</a></h3>';
                    $view .= '<span>By ' . $article_author . ' - ' . date('F j, Y', $article_date) . ' | Article Type: ' . $article_type . ' | View Count: ' . $article_viewcount . '</span> ';
                    $view .= '<div class="data">';
                    $view .= '<p>' . character_limiter(strip_tags($article_content), 20) . '</p>';
                    $view .= '<span><a href="' . base_url('gnews/view_article/' . $article_id) . '">Continue reading >>></a></span>';
                    $view .= '</div><div class="clear"></div>';
                    echo $view;
                }
                ?>
            </div>

            <!------ PAGE LINKS ---->

            <div class="page_links">
                <div class="page_numbers">
<?php echo $links; ?>
                    <!--<ul>
                        <li><a href="#">1</a>
                        <li class="active"><a href="#">2</a>
                        <li><a href="#">3</a>
                        <li><a href="#">4</a>
                        <li><a href="#">5</a>
                        <li><a href="#">6</a>
                        <li><a href="#">... Next</a>
                    </ul>-->
                </div>
                <div class="clear"></div>
                <div class="page_bottom">
                    <p>Back To : <a href="#">Top</a> |  <a href="<?php echo base_url('gnews/home'); ?>">Home</a></p>
                </div>
            </div><!-- END LINKS-->
        </div><!-- END CONTENT -->

