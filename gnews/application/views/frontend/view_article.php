
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="wrap">
    <div class="main">
        <div class="content">
            <?php
            foreach ($article_details as $row) {
                $title = $row->Title;
                $content = $row->Content;
                $author = $row->Author;
                $article_date = strtotime($row->Date_posted);
                $article_type = $row->Name;
                $article_viewcount = $row->ViewCounter;
            }
            ?>
            <div class="box1">
                <h3><a href="<?php echo current_url(); ?>"><?php echo $title; ?></a></h3>
                <span>By <?php echo $author; ?> - <?php echo date('F j, Y', $article_date); ?> | Article Type: <?php echo $article_type; ?> | View Count: <?php echo $article_viewcount; ?> </span> 

                <div class="blog-data">
<?php echo $content; ?>
                </div>
                <div class="clear"></div>
            </div>			

            <div class="next_button">
                <a href="<?php echo base_url();?>">Back</a>
            </div>
            <div class="clear"> </div>
            <br>
            <!----------------- End Comment Area ----------------->	
            
            <div style="background-color: white;">
            <div class="fb-comments" data-href="<?php echo current_url();?>" data-numposts="10"   data-mobile="false"></div>
            </div>
        </div>