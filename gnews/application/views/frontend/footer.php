
<div class="sidebar">
    <div class="sidebar_top">
        <h3>Article Categories</h3>
        <div class="sidebar_top_list">
            <ul>
                <?php
                foreach ($cat_count as $cat) {

                    echo '<li><a href="' . base_url('gnews/article/' . $cat->CatID) . '"><span class="category-name">' . $cat->Name . '</span> <span class="count">(' . $cat->RowCount . ')</span><div class="clear"></div></a></li>';
                }
                ?>
                   <!--<li><a href="#"><span class="category-name">Music</span> <span class="count">(22)</span><div class="clear"></div></a></li>
                   <li><a href="#"><span class="category-name">Technology</span> <span class="count">(44)</span><div class="clear"></div></a></li>
                   <li><a href="#"><span class="category-name">Nature</span> <span class="count">(32)</span><div class="clear"></div></a></li>
                   <li><a href="#"><span class="category-name">Fashion</span> <span class="count">(15)</span><div class="clear"></div></a></li>-->
            </ul>
        </div>

    </div>



    <div class="clear"></div>
</div>
<div class="clear"></div>
</div>
</div>


<div class="footer">
    <div class="footer_grides"> 
        <div class="wrap">  	    	
            <div class="footer_grid1">

            </div>			
            <div class="footer_grid2">
                <h3>Page Layout</h3>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>						     		
                    <li><a href="#">Contact</a></li>
                </ul>
                <div class="grid2_bottom">
                    <h3>Newsletters Signup</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.......</p>
                    <div class="signup">
                        <form>
                            <input type="text" value="Enter your e-mail here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your e-mail here';"><input type="submit" value="Sign up">
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer_grid3">
                <h3>Follow</h3>
                <div class="social_icons">
                    <div class="img_list">
                        <ul>
                            <li><img src="images/facebook.png" alt="" /><a href="#">Facebook</a></li>
                            <li><img src="images/google%2b.png" alt="" /><a href="#">Google Plus</a></li>
                            <li><img src="images/twitter.png" alt="" /><a href="#">Twitter</a></li>
                            <li><img src="images/linkedin.png" alt="" /><a href="#">Likedin</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="copy_right">
        <p> © 2013 Share_Blog. All Rights Reserved | Design by  <a href="http://w3layouts.com/">W3Layouts</a> </p>
    </div>	 
</div>
</body>

<!-- Mirrored from p.w3layouts.com/demos/share_blog/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Aug 2016 07:05:10 GMT -->
</html>