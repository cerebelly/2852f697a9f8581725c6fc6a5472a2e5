<!DOCTYPE html>
<html class="bg-white">
    <head>
        <meta charset="UTF-8">
        <title>Government News | Log In</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url('assets/backend/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url('assets/backend/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/backend/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-white">

        <div class="form-box " id="login-box">
            <div class="header bg-blue">Administrator Sign In</div>
            <?php echo form_open('adminbackend/login'); ?>
                <div class="body bg-gray">
                    <br>
                    <?php echo $login_error;?>
                    <?php echo form_error('username');?>
                    <?php echo form_error('password');?>
                    <div class="form-group">
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-users"></i></span>
                        <input type="text" name="username" value="<?php echo set_value('username');?>" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" value="<?php echo set_value('password');?>" class="form-control" placeholder="Password"/>
                        </div> 
                    </div>
                </div>
                <div class="footer bg-gray">                                                               
                    <button type="submit" class="btn bg-blue btn-block">Log In</button>  
                    
                    <p><a href="#">I forgot my password</a></p>
                </div>
            </form>

            
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('assets/backend/js/bootstrap.min.js');?>" type="text/javascript"></script>        

    </body>
</html>