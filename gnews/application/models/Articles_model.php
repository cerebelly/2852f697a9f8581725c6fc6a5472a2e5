<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Article_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /* GET ARTICLE CATEGORIES */

    function get_acat() {
        $query = $this->db->query("SELECT DISTINCT tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID");

        foreach ($query->result() as $row) {
            $data[] = $row;
        }

        return $data;
    }

    function get_acat_count() {
        $data = $this->get_acat();
        foreach ($data as $rows) {
            $query = $this->db->query("SELECT Count(tbl_article.CatID) AS RowCount, tbl_article_category.`Name`, tbl_article.CatID FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID WHERE tbl_article_category.`Name` = '" . $rows->Name . "'");
            $rc = $query->row();
            $rcount[] = $rc;
        }
        return $rcount;
    }

    /* END GET ARTICLE CATEGORIES */

    /* GET ALL ARTICLE */

    /*function article_count() {
        $query = $this->db->query("SELECT * FROM tbl_article;");
        $row = $query->num_rows();
        return $row;
    }*/

     function article_count() {
        $query = $this->db->query("SELECT * FROM contents;");
        $row = $query->num_rows();
        return $row;
    }

    /*function get_article($limit, $start) {
        $query = $this->db->query("SELECT tbl_article.ArticleID, tbl_article.Title, tbl_article.Content, tbl_article.Author, tbl_article.ViewCounter, tbl_article.Date_posted, tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID ORDER BY ArticleID DESC LIMIT " . $limit . " OFFSET " . $start . ";");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }*/

    function get_article($limit, $start) {
        $query = $this->db->query("SELECT *  FROM  contents order by date_created DESC LIMIT " . $limit . " OFFSET " . $start . ";");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /* END GET ALL ARTICLE */

    /* GET ALL ARTICLE BY CATEGORY */

    function article_category_count($catid) {
        $query = $this->db->query("SELECT * FROM tbl_article WHERE CatID = " . $catid . ";");
        $row = $query->num_rows();
        return $row;
    }

    function get_category_article($limit, $start, $catid) {
        $query = $this->db->query("SELECT tbl_article.ArticleID, tbl_article.Title, tbl_article.Content, tbl_article.Author, tbl_article.ViewCounter, tbl_article.Date_posted, tbl_article_category.`Name` FROM tbl_article INNER JOIN tbl_article_category ON tbl_article.CatID = tbl_article_category.CatID WHERE tbl_article.CatID = '" . $catid . "' ORDER BY ArticleID DESC LIMIT " . $limit . " OFFSET " . $start . ";");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_generated_pass($chars_max = 30, $use_upper_case = true, $include_numbers = true, $include_special_chars = false) {
        $length = $chars_max;
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if ($include_numbers) {
            $selection .= "1234567890";
        }
        if ($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $password .= $current_letter;
        }

        return $password;
    }

    /* END GET ALL ARTICLE BY CATEGORY */

    /* GET ALL SEARCHED ARTICLE */

    function search_article_count($search) {
        $query = $this->db->query("SELECT * FROM contents WHERE title LIKE '%".$search."%';");
        $row = $query->num_rows();
        return $row;
    }

    function get_search_article($limit, $start,$search) {
        $query = $this->db->query("SELECT * FROM contents where title like  '%".$search."%' ORDER BY date_created DESC LIMIT " . $limit . " OFFSET " . $start . ";");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

  

    /* END GET ALL SEARCHED ARTICLE */
}
