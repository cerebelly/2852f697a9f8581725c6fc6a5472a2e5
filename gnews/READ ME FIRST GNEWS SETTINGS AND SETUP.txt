Editing Database Settings
1. Go to folder "application/config/"
2. Open file database.php
3. Edit database settings "hostname,username,password,database"
4. Save

Opening Website Front end
1. Create website folder & Copy all files to htdocs of your xampp or wamp server if running on web server copy all files to public_html folder
2. open web browser and enter link http://url/foldername/ ex.(http://127.0.0.1/gnews) 

Opening User Back End
1. Enter link http://url/foldername/userbackend ex.(http://127.0.0.1/gnews/userbackend)

Opening Admin Back End
1. Enter link http://url/foldername/adminbackend ex.(http://127.0.0.1/gnews/adminbackend)

LOGIN CREDENTIALS
Admin Backend 
username: admin
password: admin1234

User Backend
username: user
password: user1234

Notes: ADMINISTRATOR ACCOUNT CAN BE USED ON USER Backend

DEFAULT DATABASE NAME: gnews_db