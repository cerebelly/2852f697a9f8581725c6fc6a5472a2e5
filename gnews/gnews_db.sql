-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: gnews_db
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `complaints_suggestion`
--

DROP TABLE IF EXISTS `complaints_suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complaints_suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complaints_suggestion`
--

LOCK TABLES `complaints_suggestion` WRITE;
/*!40000 ALTER TABLE `complaints_suggestion` DISABLE KEYS */;
INSERT INTO `complaints_suggestion` VALUES (1,'TITLTE ','<p>TITLTE TITLTE TITLTE TITLTE TITLTE TITLTE </p>\r\n\r\n<p>TITLTE TITLTE TITLTE TITLTE TITLTE </p>\r\n\r\n<p>TITLTE TITLTE TITLTE TITLTE </p>\r\n','2016-12-10 08:00:26'),(2,'TITLTE ','<p>TITLTE TITLTE TITLTE TITLTE TITLTE TITLTE </p>\r\n\r\n<p>TITLTE TITLTE TITLTE TITLTE TITLTE </p>\r\n\r\n<p>TITLTE TITLTE TITLTE TITLTE </p>\r\n','2016-12-10 08:00:37');
/*!40000 ALTER TABLE `complaints_suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `title` text NOT NULL,
  `path` text NOT NULL,
  `site` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES ('Naia passengers can enjoy free WiFi until Oct. 10','http://newsinfo.inquirer.net/818448/naia-passengers-can-enjoy-free-wifi-until-oct-10','naia','2016-10-01 04:57:17'),('Travel time to Naia Terminals 1, 2 cut','http://newsinfo.inquirer.net/817755/travel-time-to-naia-terminals-1-2-cut','naia','2016-10-01 04:57:17'),('','http://newsinfo.inquirer.net/source/inquirer-net','naia','2016-10-01 04:57:17'),('Saudia passengers disembark after plane isolated at Naia runway','http://newsinfo.inquirer.net/817269/saudia-passengers-disembark-after-plane-isolated-at-naia-runway','naia','2016-10-01 04:57:17'),('Saudia plane isolated at Naia runway','http://newsinfo.inquirer.net/817213/saudia-plane-isolated-at-naia-runway','naia','2016-10-01 04:57:17'),('Nothing wrong with height of â€˜obstructiveâ€™ condo, says developer','http://newsinfo.inquirer.net/815913/nothing-wrong-with-height-of-obstructive-condo-says-developer','naia','2016-10-01 04:57:17'),('â€˜Salisi gangâ€™ strikes at Naia, victimizes passenger','http://newsinfo.inquirer.net/815796/salisi-gang-strikes-at-naia-victimizes-passenger','naia','2016-10-01 04:57:17'),('Condominium blamed for obstructing Naia flight path','http://newsinfo.inquirer.net/815201/condominium-blamed-for-obstructing-naia-flight-path','naia','2016-10-01 04:57:17'),('Letâ€™s be patriots','http://newsinfo.inquirer.net/813767/lets-be-patriots','naia','2016-10-01 04:57:17'),('PAL cancels certain flights at NAIA effective Sept. 1','http://newsinfo.inquirer.net/811679/pal-cancels-certain-flights-at-naia-effective-sept-1','naia','2016-10-01 04:57:17'),('Security firm claims headway vs Naia bag pilferage; two handlers caught','http://newsinfo.inquirer.net/806859/security-firm-claims-headway-vs-naia-bag-pilferage-two-handlers-caught','naia','2016-10-01 04:57:21'),('Bad weather prompts diversion of flights to Clark airport','http://newsinfo.inquirer.net/805832/bad-weather-prompts-diversion-of-flights-to-clark-airport','naia','2016-10-01 04:57:21'),('Flights cancelled, diverted to Clark due to bad weather','http://newsinfo.inquirer.net/805857/flights-cancelled-diverted-to-clark-due-to-bad-weather','naia','2016-10-01 04:57:21'),('2 NAIA security men probed on bribes; 35 others rounded up','http://newsinfo.inquirer.net/800324/2-naia-security-men-probed-on-bribes-35-tagged-over-illegal-vehicles','naia','2016-10-01 04:57:21'),('Amid Naia congestion, Gordon pushes for Clark, Subic development','http://newsinfo.inquirer.net/797006/amid-naia-congestion-gordon-pushes-for-clark-subic-development','naia','2016-10-01 04:57:21'),('Naia allows regular taxis to pick up passengers in arrival area','http://newsinfo.inquirer.net/796863/naia-allows-regular-taxis-to-pick-up-passengers-in-arrival-area','naia','2016-10-01 04:57:21'),('Naia repairs force 8 planes to divert to Clark','http://newsinfo.inquirer.net/796798/naia-repairs-forces-8-planes-to-divert-to-clark','naia','2016-10-01 04:57:21'),('Metro Briefs: Regular cabs can pick up Naia arrivals','http://newsinfo.inquirer.net/796342/metro-briefs-regular-cabs-can-pick-up-naia-arrivals','naia','2016-10-01 04:57:21'),('Plan to develop new airport in Cavite lauded','http://newsinfo.inquirer.net/795662/plan-to-develop-new-airport-in-cavite-lauded','naia','2016-10-01 04:57:21'),('â€˜Butchoyâ€™ forces planes to divert to Clark','http://newsinfo.inquirer.net/795015/butchoy-forces-planes-to-divert-to-clark','naia','2016-10-01 04:57:21'),('LIST: Canceled flights on July 7, 2016 due to â€˜Butchoyâ€™','http://newsinfo.inquirer.net/794802/list-canceled-flights-on-july-7-2016-due-to-butchoy','naia','2016-10-01 04:57:25'),('P67-M worth of security scanners missing from airports','http://newsinfo.inquirer.net/791748/p67-m-worth-of-security-scanners-missing-from-airports','naia','2016-10-01 04:57:25'),('Big drop in â€˜tanim-balaâ€™ incidents at Naia noted','http://newsinfo.inquirer.net/788287/big-drop-in-tanim-bala-incidents-at-naia-noted','naia','2016-10-01 04:57:25'),('P150-M Naia body scanners still hardly used','http://newsinfo.inquirer.net/787752/p150-m-naia-body-scanners-still-hardly-used','naia','2016-10-01 04:57:25'),('','http://newsinfo.inquirer.net/byline/marlon-ramos','naia','2016-10-01 04:57:25'),('Purisima surrenders wearing shorts','http://newsinfo.inquirer.net/786763/purisima-surrenders-wearing-shorts','naia','2016-10-01 04:57:25'),('DOJ asked to transfer bullet case to Pasay prosecutorsâ€™ office','http://newsinfo.inquirer.net/785285/doj-asked-to-transfer-bullet-case-to-pasay-prosecutors-office','naia','2016-10-01 04:57:25'),('Septuagenarian couple seek dismissal of bullet case','http://newsinfo.inquirer.net/785123/tanim-bala-3','naia','2016-10-01 04:57:25'),('Contractual workers pin hopes on next President','http://newsinfo.inquirer.net/784102/contractual-workers-pin-hopes-on-next-president','naia','2016-10-01 04:57:25'),('Grassfire hits area near Naia Terminal 3','http://newsinfo.inquirer.net/783494/grassfire-hits-area-near-naia-terminal-3','naia','2016-10-01 04:57:25'),('In defense of â€˜bullet-carrier,â€™ PAO sues 3 at Naia','http://newsinfo.inquirer.net/783075/in-defense-of-bullet-carrier-pao-sues-3-at-naia','naia','2016-10-01 04:57:28'),('Naia passenger carrying 3 bullets charged, released','http://newsinfo.inquirer.net/782608/naia-passenger-carrying-3-bullets-charged-released','naia','2016-10-01 04:57:28'),('DOJ chief: â€˜Tanim-balaâ€™ being politicized','http://newsinfo.inquirer.net/781869/doj-chief-tanim-bala-being-politicized','naia','2016-10-01 04:57:28'),('Palace vows to protect travelers against NAIA opportunists','http://newsinfo.inquirer.net/780727/malacanang-assures-public-amid-new-tanim-bala-allegation','naia','2016-10-01 04:57:28'),('â€˜Tanim-balaâ€™ back? Woman, 75, stopped at NAIA for bullet in bag','http://newsinfo.inquirer.net/780663/tanim-bala-back-woman-75-stopped-at-naia-for-bullet-in-bag','naia','2016-10-01 04:57:28'),('','http://newsinfo.inquirer.net/byline/karl-angelica-r-ocampo','naia','2016-10-01 04:57:28'),('Poe camp: Naia-3 power outage a national embarrassment','http://newsinfo.inquirer.net/777587/poe-camp-naia-3-power-outage-a-national-embarrassment','naia','2016-10-01 04:57:28'),('GrabCar taxis now available at NAIA','http://newsinfo.inquirer.net/773637/grabcar-taxis-now-available-at-naia','naia','2016-10-01 04:57:29'),('RCBC branch manager stopped from leaving PH','http://newsinfo.inquirer.net/772887/rcbc-branch-manager-stopped-from-leaving-ph','naia','2016-10-01 04:57:29'),('Airport buses offer rides to Makati, Roxas Blvd.','http://newsinfo.inquirer.net/765836/airport-buses-offer-rides-to-makati-roxas-blvd','naia','2016-10-01 04:57:29'),('DoTC, LTFRB launch 24-hour airport bus service; fare at P300','http://newsinfo.inquirer.net/765805/dotc-launches-premium-airport-bus-service-fare-pegged-at-p300','naia','2016-10-01 04:57:32'),('Another tale of honesty at Naia','http://newsinfo.inquirer.net/763329/another-tale-of-honesty-at-naia','naia','2016-10-01 04:57:32'),('OTS, Aviation Security Group personnel warned if â€˜tanim-balaâ€™ continues','http://newsinfo.inquirer.net/760579/ots-aviation-security-group-personnel-warned-if-tanim-bala-continues','naia','2016-10-01 04:57:32'),('Only 1,500 taxi cabs for 10,000 tourists arriving in PH daily','http://newsinfo.inquirer.net/758633/only-1500-taxi-cabs-for-10000-tourists-arriving-in-ph-daily','naia','2016-10-01 04:57:32'),('Negotiated deal for P486M CCTV system at NAIA eyed','http://newsinfo.inquirer.net/758305/negotiated-deal-for-p486m-cctv-system-at-naia-eyed','naia','2016-10-01 04:57:32'),('Solve problem on â€˜overchargingâ€™ of airport taxis, Drilon urges execs','http://newsinfo.inquirer.net/755288/solve-problem-on-overcharging-of-airport-taxis-drilon-urges-execs','naia','2016-10-01 04:57:32'),('Stop collecting terminal fee from OFWs, migrant group urges govâ€™t','http://newsinfo.inquirer.net/755145/stop-collecting-terminal-fee-from-ofws-migrant-group-urges-govt','naia','2016-10-01 04:57:32'),('Metro Briefs: 20th birthday turns out to be manâ€™s last','http://newsinfo.inquirer.net/754581/metro-briefs-20th-birthday-turns-out-to-be-mans-last','naia','2016-10-01 04:57:32'),('Work on send-off lounge at Naia may start in February','http://newsinfo.inquirer.net/754145/work-on-send-off-lounge-at-naia-may-start-in-february','naia','2016-10-01 04:57:32'),('Donâ€™t inconvenience Christmas travelers, transport execs told','http://newsinfo.inquirer.net/749004/dont-inconvenience-christmas-travelers-transport-execs-told','naia','2016-10-01 04:57:33'),('Tummy ache gives away Venezuelan drug mule','http://newsinfo.inquirer.net/747986/tummy-ache-gives-away-venezuelan-drug-mule','naia','2016-10-01 04:57:36'),('Immigrationâ€™s Mison on way out','http://newsinfo.inquirer.net/746880/immigrations-mison-on-way-out','naia','2016-10-01 04:57:36'),('No top official investigated over â€˜tanim balaâ€™ racket','http://newsinfo.inquirer.net/746458/no-top-official-investigated-over-tanim-bala-racket','naia','2016-10-01 04:57:36'),('Criminal complaint filed vs OTS, Avsegroup members over â€˜tanim-balaâ€™','http://newsinfo.inquirer.net/746398/criminal-complaint-filed-vs-avsegroup-over-laglag-bala','naia','2016-10-01 04:57:36'),('â€˜Oplan Lakbay Pasko 2015â€™ to ensure hassle-free flights','http://newsinfo.inquirer.net/746248/oplan-lakbay-pasko-2015-to-ensure-hassle-free-flights','naia','2016-10-01 04:57:36'),('Woman nabbed at Naia for 500 grams of â€˜shabuâ€™','http://newsinfo.inquirer.net/746244/woman-nabbed-at-naia-for-500-grams-of-shabu','naia','2016-10-01 04:57:36'),('Naia eyes deployment of own traffic aides','http://newsinfo.inquirer.net/743498/naia-eyes-deployment-of-own-traffic-aides','naia','2016-10-01 04:57:36'),('MIAA to field traffic aides to decongest roads towards NAIA','http://newsinfo.inquirer.net/743467/miaa-to-field-traffic-aides-to-decongest-roads-towards-naia','naia','2016-10-01 04:57:36'),('For bullet carriersâ€™ cases, PNP puts ballistics test teams right at Naia','http://newsinfo.inquirer.net/743361/for-bullet-carriers-cases-pnp-puts-ballistics-test-teams-right-at-naia','naia','2016-10-01 04:57:36'),('House panel wants master plan for Clark airport as alternative to Naia','http://newsinfo.inquirer.net/742356/house-panel-wants-master-plan-for-clark-airport-as-alternative-to-naia','naia','2016-10-01 04:57:36'),('Mystery booths at Naia puzzle passengers','http://newsinfo.inquirer.net/742179/mystery-booths-at-naia-puzzle-passengers','naia','2016-10-01 04:57:37'),('Bishop: â€˜Heartlessâ€™ Aquino twisting truth with â€˜tanim-balaâ€™ comment','http://newsinfo.inquirer.net/742023/bishop-heartless-aquino-twisting-truth-with-tanim-bala-comment','naia','2016-10-01 04:57:37'),('Hassled at Naia: Passengerâ€™s 1991 ordeal made fresh by â€˜tanim-balaâ€™','http://newsinfo.inquirer.net/741425/hassled-at-naia-passengers-1991-ordeal-made-fresh-by-tanim-bala','naia','2016-10-01 04:57:37'),('Foreigners in Apec rally: â€˜Tanim-balaâ€™ scarier than riot cops','http://newsinfo.inquirer.net/741077/foreigners-in-apec-rally-tanim-bala-scarier-than-ph-cops','naia','2016-10-01 04:57:37'),('61-year-old woman found with 3 bullets in Naia released','http://newsinfo.inquirer.net/741006/61-year-old-woman-found-with-3-bullets-in-naia-released','naia','2016-10-01 04:57:37'),('Round-the-clock prosecutors at Naia for Apec','http://newsinfo.inquirer.net/740392/round-the-clock-prosecutors-at-naia-for-apec','naia','2016-10-01 04:57:37'),('Prosecutors to work longer hours at Naia','http://newsinfo.inquirer.net/739852/prosecutors-to-work-longer-hours-at-naia','naia','2016-10-01 04:57:37'),('Most prosecutors lack common sense','http://newsinfo.inquirer.net/739500/most-prosecutors-lack-common-sense','naia','2016-10-01 04:57:37'),('To charge or not, DOTC to study idea','http://newsinfo.inquirer.net/739262/to-charge-or-not-dotc-to-study-idea','naia','2016-10-01 04:57:37'),('â€˜Last lookâ€™ booths to be set up in Naia','http://newsinfo.inquirer.net/739260/last-look-booths-to-be-set-up-in-naia','naia','2016-10-01 04:57:37'),('Case vs HK-bound OFW dismissed','http://newsinfo.inquirer.net/739258/case-vs-hk-bound-ofw-dismissed','naia','2016-10-01 04:57:41'),('Naia cops asked US missionary to pay P30K','http://newsinfo.inquirer.net/739234/naia-cops-asked-us-missionary-to-pay-p30k','naia','2016-10-01 04:57:41'),('Flights of small planes at NAIA restricted during APEC','http://newsinfo.inquirer.net/739190/flights-of-small-planes-at-naia-restricted-during-apec','naia','2016-10-01 04:57:41'),('Abaya admits lapses in filing cases vs innocent â€˜tanim balaâ€™ victims','http://newsinfo.inquirer.net/739178/abaya-admits-lapses-in-filing-cases-vs-innocent-tanim-bala-victims','naia','2016-10-01 04:57:41'),('Honrado on â€˜tanim-balaâ€™ menace: â€˜Iâ€™m not in the loop on filing of casesâ€™','http://newsinfo.inquirer.net/739162/honrado-admits-doing-nothing-to-look-into-tanim-bala-menace','naia','2016-10-01 04:57:41'),('â€˜Tanim-balaâ€™ victims detail extortion try at Naia in Senate probe','http://newsinfo.inquirer.net/739153/tanim-bala-victims-detail-extortion-try-at-naia-in-senate-probe','naia','2016-10-01 04:57:41'),('More people still caught with bullets at airport','http://newsinfo.inquirer.net/738900/more-people-still-caught-with-bullets-at-airport','naia','2016-10-01 04:57:41'),('Duterte to Aquino: I demand action','http://newsinfo.inquirer.net/738873/duterte-to-aquino-i-demand-action','naia','2016-10-01 04:57:41'),('Seaman in hot water over â€˜tanim-balaâ€™ claim on Facebook','http://newsinfo.inquirer.net/738543/seaman-in-hot-water-over-tanim-bala-claim-on-facebook','naia','2016-10-01 04:57:41'),('NUJP hits media restrictions at Naia amid â€˜tanim-balaâ€™ scam','http://newsinfo.inquirer.net/738471/nujp-hits-media-restrictions-at-naia-amid-tanim-bala-scam','naia','2016-10-01 04:57:41'),('Case vs 72-year-old â€˜tanim-balaâ€™ suspect dismissed','http://newsinfo.inquirer.net/738415/case-vs-72-year-old-tanim-bala-suspect-dismissed','naia','2016-10-01 04:57:44'),('Senate to begin â€˜tanim-balaâ€™ probe; Abaya among resource persons','http://newsinfo.inquirer.net/738372/senate-to-begin-tanim-bala-probe-abaya-among-resource-persons','naia','2016-10-01 04:57:45'),('MIAA workers tapped to spot extortionists at NAIA; Iloilo airport get CCTVs','http://newsinfo.inquirer.net/737967/miaa-workers-tapped-to-spot-extortionists-at-naia-iloilo-airport-get-cctvs','naia','2016-10-01 04:57:45'),('Roads leading to Naia remain open during Apec','http://newsinfo.inquirer.net/737839/roads-leading-to-naia-remain-open-during-apec','naia','2016-10-01 04:57:45'),('An official who is an alley cat','http://newsinfo.inquirer.net/737623/an-official-who-is-an-alley-cat','naia','2016-10-01 04:57:45'),('Citizen complaints, not Apec, prompt action on â€˜tanim-balaâ€™â€“Palace','http://newsinfo.inquirer.net/736943/citizen-complaints-not-apec-prompt-action-on-tanim-bala-palace','naia','2016-10-01 04:57:45'),('DOJ creates task force to probe â€˜Tanim-Balaâ€™ scam at Naia','http://newsinfo.inquirer.net/736856/nbi-creates-task-force-to-probe-tanim-bala-scam-at-naia','naia','2016-10-01 04:57:45'),('','http://newsinfo.inquirer.net/byline/yuji-vincent-gonzales','naia','2016-10-01 04:57:45'),('Abaya: Perpetrators could face life imprisonment for â€˜tanim-balaâ€™','http://newsinfo.inquirer.net/736770/abaya-perpetrators-could-face-life-imprisonment-for-tanim-bala','naia','2016-10-01 04:57:45'),('Curiouser: 4 more caught; 3 say bullets are â€˜charmsâ€™','http://newsinfo.inquirer.net/736670/curiouser-4-more-caught-3-say-bullets-are-charms','naia','2016-10-01 04:57:45'),('NBI ordered to probe â€˜tanim-balaâ€™ scam','http://newsinfo.inquirer.net/736607/nbi-ordered-to-probe-tanim-bala-scam','naia','2016-10-01 04:57:48'),('4 more arrested at NAIA for carrying bullets in bags','http://newsinfo.inquirer.net/736603/4-more-arrested-at-naia-for-carrying-bullets-in-bags','naia','2016-10-01 04:57:48'),('Palace: Govâ€™t resolved to fix â€˜tanim-balaâ€™ incidents','http://newsinfo.inquirer.net/736584/palace-govt-resolved-to-fix-tanim-bala-incidents','naia','2016-10-01 04:57:48'),('Escudero: Timing, frequency of â€˜tanim-balaâ€™ â€˜questionableâ€™','http://newsinfo.inquirer.net/736510/escudero-timing-frequency-of-tanim-bala-questionable','naia','2016-10-01 04:57:48'),('Cayetano files complaint vs Abaya, airport execs over â€˜tanim-balaâ€™','http://newsinfo.inquirer.net/736477/cayetano-files-complaint-vs-airport-execs-over-tanim-bala-racket','naia','2016-10-01 04:57:48'),('Avsegroup dismisses extortion racket report','http://newsinfo.inquirer.net/736361/avsegroup-dismisses-extortion-racket-report','naia','2016-10-01 04:57:48'),('Porters bear brunt of â€˜tanim-balaâ€™ fallout','http://newsinfo.inquirer.net/736372/porters-get-brunt-of-tanim-bala-fallout','naia','2016-10-01 04:57:48'),('','http://newsinfo.inquirer.net/byline/jeannette-i-andrade','naia','2016-10-01 04:57:48'),('','http://newsinfo.inquirer.net/byline/jeannette-i-andrade','naia','2016-10-01 04:57:48'),('Aquino orders probe of â€˜tanim balaâ€™ racket amid outcry','http://newsinfo.inquirer.net/736010/aquino-orders-probe-of-tanim-bala-racket-amid-outcry','naia','2016-10-01 04:57:49'),('â€˜Laglag-balaâ€™ raised with Aquino, Abaya; CCTVs to be installedâ€“Palace','http://newsinfo.inquirer.net/735934/laglag-bala-raised-with-aquino-abaya-cctvs-to-be-installed-palace','naia','2016-10-01 04:57:52'),('Ballistic Miriam joins calls for â€˜tanim-bala scamâ€™ probe','http://newsinfo.inquirer.net/735745/ballistic-miriam-joins-calls-for-tanim-bala-scam-probe','naia','2016-10-01 04:57:52'),('Brunei-bound aircraft returns to NAIA after engine conks out','http://newsinfo.inquirer.net/733447/brunei-bound-aircraft-returns-to-naia-after-engine-conks-out','naia','2016-10-01 04:57:52'),('List: Canceled flights on October 19, Monday, due to â€˜Landoâ€™','http://newsinfo.inquirer.net/732545/list-cancelled-flights-on-october-19-monday-due-to-lando','naia','2016-10-01 04:57:52'),('â€˜Turbulentâ€™ landing for pilot','http://newsinfo.inquirer.net/729588/turbulent-landing-for-pilot','naia','2016-10-01 04:57:52'),('â€˜Watching the watchersâ€™ Naia fix vs â€˜laglag-balaâ€™ rap','http://newsinfo.inquirer.net/728525/watching-the-watchers-naia-fix-vs-laglag-bala-rap','naia','2016-10-01 04:57:52'),('Airport security claim on find raises doubts','http://newsinfo.inquirer.net/727601/airport-security-claim-on-find-raises-doubts','naia','2016-10-01 04:57:52'),('â€˜Sex-for-flightâ€™ suspect not one of oursâ€”MIAA','http://newsinfo.inquirer.net/727597/sex-for-flight-suspect-not-one-of-ours-miaa','naia','2016-10-01 04:57:52'),('Another bullet found; Abaya suggests Naia flyers open bags','http://newsinfo.inquirer.net/727265/another-bullet-found-abaya-suggests-naia-flyers-open-bags','naia','2016-10-01 04:57:52'),('A month later, P150M Naia body scanners still unused','http://newsinfo.inquirer.net/727262/a-month-later-p150m-naia-body-scanners-still-unused','naia','2016-10-01 04:57:52'),('Other airports on lookout for bullet â€˜plantersâ€™','http://newsinfo.inquirer.net/725844/other-airports-on-lookout-for-bullet-planters','naia','2016-10-01 04:57:53'),('â€˜Bullet not planted,â€™ says Naia exec, but US man says â€˜they canâ€™t lie to Godâ€™','http://newsinfo.inquirer.net/725177/bullet-not-planted-says-naia-exec-but-us-man-says-they-cant-lie-to-god','naia','2016-10-01 04:57:53'),('Another Naia flyer complains bullet in bag â€˜plantedâ€™','http://newsinfo.inquirer.net/724782/another-naia-flyer-complains-bullet-in-bag-planted','naia','2016-10-01 04:57:53'),('2 Naia security screening officers probed for alleged â€˜kotongâ€™','http://newsinfo.inquirer.net/724286/2-naia-security-screening-officers-under-probe-for-alleged-kotong','naia','2016-10-01 04:57:53'),('Airport authority vows few delays during Apec','http://newsinfo.inquirer.net/724105/airport-authority-vows-few-delays-during-apec','naia','2016-10-01 04:57:53'),('NAIA Expressway, Skyway 3, LRT 2 to be delayed; traffic to worsenâ€“MMDA','http://newsinfo.inquirer.net/721897/naia-expressway-skyway-3-lrt-2-to-be-delayed-traffic-to-worsen-mmda','naia','2016-10-01 04:57:53'),('Naia flood control system in place, says airport exec','http://newsinfo.inquirer.net/721715/naia-flood-control-system-in-place-says-airport-exec','naia','2016-10-01 04:57:53'),('Pilot fined P20K for flying helicopter in NAIAâ€™s restricted zone','http://newsinfo.inquirer.net/721076/pilot-fined-p20k-for-flying-helicopter-in-naias-restricted-zone','naia','2016-10-01 04:57:53'),('Plane returns to Naia after tire comes off','http://newsinfo.inquirer.net/720478/plane-returns-to-naia-after-tire-comes-off','naia','2016-10-01 04:57:53'),('MIAA to finish all NAIA prep work for APEC on Nov. 8','http://newsinfo.inquirer.net/720381/miaa-to-finish-all-naia-prep-work-for-apec-on-nov-8','naia','2016-10-01 04:57:53'),('PAL beefing up airport security','http://newsinfo.inquirer.net/719610/pal-beefing-up-airport-security','naia','2016-10-01 04:57:57'),('Rough Naia landing hurts 40 passengers','http://newsinfo.inquirer.net/719608/rough-naia-landing-hurts-40-passengers','naia','2016-10-01 04:57:57'),('Uber, GrabCar, other TNCs welcome to operate at Naia','http://newsinfo.inquirer.net/717770/uber-grabcar-other-tncs-welcome-to-operate-at-naia','naia','2016-10-01 04:57:57'),('Aquino creates group to study NAIA terminal fee integration, exemption','http://newsinfo.inquirer.net/716157/aquino-creates-group-to-study-naia-terminal-fee-integration-exemption','naia','2016-10-01 04:57:57'),('9 domestic flights cancelled due to â€˜Inengâ€™','http://newsinfo.inquirer.net/715607/9-domestic-flights-cancelled-due-to-ineng','naia','2016-10-01 04:57:57'),('Flights cancelled due to Typhoon â€˜Inengâ€™','http://newsinfo.inquirer.net/715177/northern-luzon-flights-cancelled-due-to-typhoon-ineng','naia','2016-10-01 04:57:57'),('NAIA 4 is now all-domestic','http://newsinfo.inquirer.net/713454/naia-4-is-now-all-domestic','naia','2016-10-01 04:57:57'),('First Naia shuttle bus service provider a Lina firm','http://newsinfo.inquirer.net/712168/first-naia-shuttle-bus-service-provider-a-lina-firm','naia','2016-10-01 04:57:57'),('New Naia body search wonâ€™t leave you naked','http://newsinfo.inquirer.net/712069/full-body-scanners-at-naia-can-spot-objects-but-wont-leave-people-naked','naia','2016-10-01 04:57:57'),('Airport execs still yearning for hi-tech CCTV system','http://newsinfo.inquirer.net/710848/airport-execs-still-yearning-for-hi-tech-cctv-system','naia','2016-10-01 04:57:57'),('Newly-opened lounge in NAIA 3 gives uplifting experience for travelers','http://newsinfo.inquirer.net/709528/newly-opened-lounge-in-naia-3-gives-uplifting-experience-for-travelers','naia','2016-10-01 04:58:01'),('Cebu Pacific plane from Caticlan allegedly struck by lightning','http://newsinfo.inquirer.net/708497/cebu-pacific-flight-from-kalibo-allegedly-hit-by-lightning','naia','2016-10-01 04:58:01'),('NAIA 4 will be for domestic flights by mid-August','http://newsinfo.inquirer.net/708169/naia-4-will-be-for-domestic-flights-by-mid-august','naia','2016-10-01 04:58:01'),('Condoning dishonesty','http://newsinfo.inquirer.net/705950/condoning-dishonesty','naia','2016-10-01 04:58:01'),('Cebu Pacific cancels flights due to bad weather','http://newsinfo.inquirer.net/704257/cebu-pacific-cancels-flights-due-to-bad-weather','naia','2016-10-01 04:58:01'),('12 flights canceled due to â€˜Falconâ€™','http://newsinfo.inquirer.net/704084/12-flights-canceled-due-to-falcon','naia','2016-10-01 04:58:01'),('LTFRB looking for bus firms to ply Naia-Roxas Blvd/Makati routes','http://newsinfo.inquirer.net/702854/ltfrb-looking-for-bus-firms-to-ply-naia-roxas-blvdmakati-routes','naia','2016-10-01 04:58:01'),('New Naia body scanners in place soon','http://newsinfo.inquirer.net/701832/new-naia-body-scanners-in-place-soon','naia','2016-10-01 04:58:01'),('BI denies claims of Filipino woman who missed flight to Singapore','http://newsinfo.inquirer.net/700779/bi-denies-claims-of-filipino-woman-who-missed-flight-to-singapore','naia','2016-10-01 04:58:01'),('Commuter train service extends to Sta. Rosa, Laguna','http://www.pnr.gov.ph/news-media/press-releases/62-commm','pnr','2016-10-01 04:58:35'),('Disposal Bid','http://www.pnr.gov.ph/about-contact-us/bids-awards/disposal-bid','pnr','2016-10-01 04:58:35'),('Disposal Supplemental Bid Bulletin','http://www.pnr.gov.ph/about-contact-us/bids-awards/disposal-supplemental-bid-bulletin','pnr','2016-10-01 04:58:35'),('Holy Week PNR Trips','http://www.pnr.gov.ph/news-media/press-releases/171-holy-week-pnr-trips','pnr','2016-10-01 04:58:35'),('Invitation to Bid','http://www.pnr.gov.ph/about-contact-us/bids-awards/invitation-to-bid','pnr','2016-10-01 04:58:35'),('KRISMAS 2015 - Train Operations\' Schedule','http://www.pnr.gov.ph/news-media/press-releases/199-krismas-2015-train-operation-schedule','pnr','2016-10-01 04:58:35'),('Moving forward to the next 50 years','http://www.pnr.gov.ph/news-media/press-releases/100-moving-forward-to-the-next-50-years','pnr','2016-10-01 04:58:36'),('Notice of Award','http://www.pnr.gov.ph/about-contact-us/bids-awards/notice-of-award','pnr','2016-10-01 04:58:36'),('PhilGEPS','http://www.pnr.gov.ph/about-contact-us/bids-awards/philgeps','pnr','2016-10-01 04:58:36'),('Photos - Bagong Ilog MOA Signing with Philippine National Railways','http://www.pnr.gov.ph/photos-bagong-ilog-moa-signing-with-philippine-national-railways','pnr','2016-10-01 04:58:36'),('Photos - Blessing and Inauguration of the new PNR Iriga, Polangui, Ligao, Travesia, Daraga and Legazpi Stations','http://www.pnr.gov.ph/photos-blessing-and-inauguration-of-the-new-pnr-iriga-polangui-ligao-travesia-daraga-and-legazpi-stations','pnr','2016-10-01 04:58:40'),('Photos - Blessing of New Train Coach from Kanto Railways Japan','http://www.pnr.gov.ph/news-media/press-releases/187-blessing-of-new-train-coach-from-kanto-railway-japan','pnr','2016-10-01 04:58:40'),('Photos - PNR Test Run with Sen. Cynthia A. Villar','http://www.pnr.gov.ph/inspection-villar','pnr','2016-10-01 04:58:40'),('Photos-Inspection Run to Calamba by PNR Management','http://www.pnr.gov.ph/photos-inspection-run-to-calamba-by-pnr-management','pnr','2016-10-01 04:58:40'),('Photos-Labor Management Meeting','http://www.pnr.gov.ph/photos-labor-management-meeting','pnr','2016-10-01 04:58:40'),('PNOY, DOTC Releases P2.3 Billion to PNR','http://www.pnr.gov.ph/news-media/press-releases/173-pnoy-dotc-realeases-p2-3-billion-to-pnr','pnr','2016-10-01 04:58:40'),('PNR Employee Injured after stoning incident','http://www.pnr.gov.ph/news-media/press-releases/65-pnr-employee-injured-after-stoning-incident','pnr','2016-10-01 04:58:40'),('PNR Holy Week Advisory','http://www.pnr.gov.ph/holy-week-pnr-trips-schedule','pnr','2016-10-01 04:58:40'),('PNR Marks 122 years with Bigger Plans for 2015','http://www.pnr.gov.ph/news-media/press-releases/103-pnr-marks-122-years-with-bigger-plans-for-2015','pnr','2016-10-01 04:58:40'),('PNR to resume trips July 23, 2015','http://www.pnr.gov.ph/news-media/press-releases/180-pnr-suspends-its-metro-manila-operations','pnr','2016-10-01 04:58:40'),('Calendar of Events','http://ltfrb.gov.ph/main/newsroomhttp://ltfrb.gov.ph/eventcal','ltfrb','2016-10-13 14:32:04'),('LTFRB Opens Airport Buses','http://ltfrb.gov.ph/main/newsroom#83','ltfrb','2016-10-13 14:32:04'),('PROVISIONAL ROLLBACK ON PUJ FARE','http://ltfrb.gov.ph/main/newsroom#82','ltfrb','2016-10-13 14:32:04'),('Court of Appeals  UPHOLDS  LTFRB Cancellation of Don Mariano Bus','http://ltfrb.gov.ph/main/newsroom#81','ltfrb','2016-10-13 14:32:04'),('LTFRB ISSUES SHOW CAUSE ORDER','http://ltfrb.gov.ph/main/newsroom#80','ltfrb','2016-10-13 14:32:04'),('GROUP ASKS LTFRB TO REVERT MINIMUM FARE','http://ltfrb.gov.ph/main/newsroom#79','ltfrb','2016-10-13 14:32:04'),('NOTICE TO ALL!!','http://ltfrb.gov.ph/main/newsroom#78','ltfrb','2016-10-13 14:32:04'),('LTFRB Suspends Victory Liner Inc.','http://ltfrb.gov.ph/main/newsroom#77','ltfrb','2016-10-13 14:32:05'),('BICOL Buses','http://ltfrb.gov.ph/main/newsroom#76','ltfrb','2016-10-13 14:32:05'),('PUB Route Rationalization','http://ltfrb.gov.ph/main/newsroom#75','ltfrb','2016-10-13 14:32:05'),('LTFRB to Require Public Utility Buses to Install GPS','http://ltfrb.gov.ph/main/newsroom#74','ltfrb','2016-10-13 14:32:05'),('LTFRB hits MMDA','http://ltfrb.gov.ph/main/newsroom#71','ltfrb','2016-10-13 14:32:05'),('Provincial Bus Routes Rationalization','http://ltfrb.gov.ph/main/newsroom#70','ltfrb','2016-10-13 14:32:05'),('Trucks for Hire','http://ltfrb.gov.ph/main/newsroom#69','ltfrb','2016-10-13 14:32:05'),('LTFRB DISTRIBUTES NEW FARE MATRIX','http://ltfrb.gov.ph/main/newsroom#68','ltfrb','2016-10-13 14:32:05'),('LTFRB 24/7 HOTLINE','http://ltfrb.gov.ph/main/newsroom#57','ltfrb','2016-10-13 14:32:05'),('LTFRB Warned PUVs against Overcharging of Fare','http://ltfrb.gov.ph/main/newsroom#54','ltfrb','2016-10-13 14:32:05'),('LTFRB grants Special Permit to PUB for Visayas Routes','http://ltfrb.gov.ph/main/newsroom#53','ltfrb','2016-10-13 14:32:05'),('LTFRB Suspends ALPS BUS','http://ltfrb.gov.ph/main/newsroom#52','ltfrb','2016-10-13 14:32:05'),('DOTC Anti-Colorum Campaign','http://ltfrb.gov.ph/main/newsroom#51','ltfrb','2016-10-13 14:32:05'),('LTFRB Suspends SUPERLINE BUS','http://ltfrb.gov.ph/main/newsroom#50','ltfrb','2016-10-13 14:32:05'),('TAXI Driver Modus Operandi','http://ltfrb.gov.ph/main/newsroom#49','ltfrb','2016-10-13 14:32:05'),('Invitation To Participate -Update','http://ltfrb.gov.ph/main/newsroom#48','ltfrb','2016-10-13 14:32:05'),('Invitation To Participate','http://ltfrb.gov.ph/main/newsroom#47','ltfrb','2016-10-13 14:32:05'),('Public Utility Bus (PUB) 15 years age limit','http://ltfrb.gov.ph/main/newsroom#46','ltfrb','2016-10-13 14:32:05'),('LTFRB Opens Airport Buses','http://ltfrb.gov.ph/main/newsroom#83','ltfrb','2016-10-13 14:32:06'),('PROVISIONAL ROLLBACK ON PUJ FARE','http://ltfrb.gov.ph/main/newsroom#82','ltfrb','2016-10-13 14:32:06'),('Court of Appeals  UPHOLDS  LTFRB Cancellation of Don Mariano Bus','http://ltfrb.gov.ph/main/newsroom#81','ltfrb','2016-10-13 14:32:06'),('LTFRB ISSUES SHOW CAUSE ORDER','http://ltfrb.gov.ph/main/newsroom#80','ltfrb','2016-10-13 14:32:06'),('GROUP ASKS LTFRB TO REVERT MINIMUM FARE','http://ltfrb.gov.ph/main/newsroom#79','ltfrb','2016-10-13 14:32:06'),('NOTICE TO ALL!!','http://ltfrb.gov.ph/main/newsroom#78','ltfrb','2016-10-13 14:32:06'),('LTFRB Suspends Victory Liner Inc.','http://ltfrb.gov.ph/main/newsroom#77','ltfrb','2016-10-13 14:32:06'),('BICOL Buses','http://ltfrb.gov.ph/main/newsroom#76','ltfrb','2016-10-13 14:32:06'),('PUB Route Rationalization','http://ltfrb.gov.ph/main/newsroom#75','ltfrb','2016-10-13 14:32:06'),('LTFRB to Require Public Utility Buses to Install GPS','http://ltfrb.gov.ph/main/newsroom#74','ltfrb','2016-10-13 14:32:06'),('LTFRB hits MMDA','http://ltfrb.gov.ph/main/newsroom#71','ltfrb','2016-10-13 14:32:06'),('Provincial Bus Routes Rationalization','http://ltfrb.gov.ph/main/newsroom#70','ltfrb','2016-10-13 14:32:06'),('Trucks for Hire','http://ltfrb.gov.ph/main/newsroom#69','ltfrb','2016-10-13 14:32:06'),('LTFRB DISTRIBUTES NEW FARE MATRIX','http://ltfrb.gov.ph/main/newsroom#68','ltfrb','2016-10-13 14:32:06'),('LTFRB 24/7 HOTLINE','http://ltfrb.gov.ph/main/newsroom#57','ltfrb','2016-10-13 14:32:06'),('LTFRB Warned PUVs against Overcharging of Fare','http://ltfrb.gov.ph/main/newsroom#54','ltfrb','2016-10-13 14:32:06'),('LTFRB grants Special Permit to PUB for Visayas Routes','http://ltfrb.gov.ph/main/newsroom#53','ltfrb','2016-10-13 14:32:06'),('LTFRB Suspends ALPS BUS','http://ltfrb.gov.ph/main/newsroom#52','ltfrb','2016-10-13 14:32:06'),('DOTC Anti-Colorum Campaign','http://ltfrb.gov.ph/main/newsroom#51','ltfrb','2016-10-13 14:32:06'),('LTFRB Suspends SUPERLINE BUS','http://ltfrb.gov.ph/main/newsroom#50','ltfrb','2016-10-13 14:32:06'),('TAXI Driver Modus Operandi','http://ltfrb.gov.ph/main/newsroom#49','ltfrb','2016-10-13 14:32:06'),('Invitation To Participate -Update','http://ltfrb.gov.ph/main/newsroom#48','ltfrb','2016-10-13 14:32:06'),('Invitation To Participate','http://ltfrb.gov.ph/main/newsroom#47','ltfrb','2016-10-13 14:32:06'),('Public Utility Bus (PUB) 15 years age limit','http://ltfrb.gov.ph/main/newsroom#46','ltfrb','2016-10-13 14:32:06'),('I-ACT TO SUSPEND WINDOW HOURS FOR PRIVATE VEHICLES STARTING OCTOBER 17, MORATORIUM ON ALL UTILITY WORKS TO START IN NOVEMBER\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/206-i-act-to-suspend-window-hours-for-private-vehicles-starting-october-17-moratorium-on-all-utility-works-to-start-in-november\r\n','dotc','2016-10-22 07:03:12'),('PPA tightens security noose in all ports nationwide in the wake of the Davao blast\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/205-ppa-tightens-security-noose-in-all-ports-nationwide-in-the-wake-of-the-davao-blast\r\n','dotc','2016-10-22 07:03:12'),('JAPAN BUILD COAST GUARD VESSEL PART OF PNOY ADMIN PROJECT\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/204-japan-build-coast-guard-vessel-part-of-pnoy-admin-project\r\n','dotc','2016-10-22 07:03:12'),('DOTr ASKS RAIL COMMUTERS TO RELOAD BEEP CARDS OUTSIDE TRAIN STATIONS\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/203-dotr-asks-rail-commuters-to-reload-beep-cards-outside-train-stations\r\n','dotc','2016-10-22 07:03:12'),('DOTr TAKES OVER TRAFFIC MANAGEMENT IN METRO MANILA\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/202-dotr-takes-over-traffic-management-in-metro-manila\r\n','dotc','2016-10-22 07:03:12'),('Transport Department, Smart ink deal  for free WiFi access in  key transport hubs\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/201-transport-department-smart-ink-deal-for-free-wifi-access-in-key-transport-hubs\r\n','dotc','2016-10-22 07:03:12'),('MOA Signing on Airport Upkeep\r\n','http://www.dotc.gov.ph/index.php/2014-09-02-05-01-41/2014-09-03-06-43-32/200-moa-signing-on-airport-upkeep\r\n','dotc','2016-10-22 07:03:12'),('Corporate Social Responsibility-Tree Planting','http://www.lrta.gov.ph/index.php/14-auxiliary-menu/66-corporate-social-responsibility-tree-planting','ltra','2016-10-22 07:07:40'),('LRTA Recreational Intramural Games 2016','http://www.lrta.gov.ph/index.php/14-auxiliary-menu/65-lrta-recreational-intramural-games-2016','ltra','2016-10-22 07:07:40'),('Japan Parliamentary Mission visits LRTA','http://www.lrta.gov.ph/index.php/2-uncategorised/29-japan-parliamentary-mission-visits-lrta','ltra','2016-10-22 07:07:40'),('Line 1 Resumes Full Operation After Suicide Incident','http://www.lrta.gov.ph/index.php/15-news/30-line-1-resumes-full-operation-after-suicide-incident','ltra','2016-10-22 07:07:40'),('Caregiver helps deliver a baby at LRT Central Station','http://www.lrta.gov.ph/index.php/15-news/31-caregiver-helps-deliver-a-baby-at-lrt-central-station','ltra','2016-10-22 07:07:40'),('LRTA Offers Free Rides on Independence Day','http://www.lrta.gov.ph/index.php/15-news/32-lrta-offers-free-rides-on-independence-day','ltra','2016-10-22 07:07:40'),('LRTA to Regulate Number of Passengers Entering Line 1 Stations','http://www.lrta.gov.ph/index.php/15-news/33-lrta-to-regulate-number-of-passengers-entering-line-1-stations','ltra','2016-10-22 07:07:40'),('LRTA addresses mechanical problem in Stored Value Tickets','http://www.lrta.gov.ph/index.php/15-news/34-lrta-addresses-mechanical-problem-in-stored-value-tickets','ltra','2016-10-22 07:07:41'),('Notice to the Public','http://www.lto.gov.ph/lto-advisory/202-notice-to-the-public\r\n','lto','2016-12-10 07:18:15'),('Advisory on Availability of Plastic License Cards','http://www.lto.gov.ph/lto-advisory/200-advisory-on-availability-of-plastic-license-cards\r\n','lto','2016-12-10 07:18:15'),('Advisory on 5-year License Validity Schedule of Implementation','http://www.lto.gov.ph/lto-advisory/197-advisory-on-5-year-license-validity-schedule-of-implementation\r\n','lto','2016-12-10 07:18:15'),('Notice of Disposal','http://www.lto.gov.ph/lto-advisory/192-notice-of-disposal\r\n','lto','2016-12-10 07:18:15'),('Motor Vehicle Registration Schedule based on the Plate Number','http://www.lto.gov.ph/lto-advisory/188-motor-vehicle-registration-schedule-based-on-the-plate-number\r\n','lto','2016-12-10 07:18:15'),('Driver\'s License Supporting Documents per Transactions','http://www.lto.gov.ph/lto-advisory/179-driver-s-license-supporting-per-transactions-as-per-latest-administrative-order\r\n','lto','2016-12-10 07:18:15');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `guid` varchar(100) NOT NULL,
  `timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'qweqweqwe','asdasd','50263678-3450-f714-1d21-349bdd9cafbf',1472739322),(2,'sample lance','lance','6083b52a-4e28-036c-d474-2feb518703bb',1472739641),(3,'qweqweqwe','asdasd','b622b745-f13c-e7f5-dbf1-abb2caaca306',1472740142),(4,'hello world','Aaron Angeles','5494a15b-5887-8763-4e73-be823f7fa5bb',1472740664),(5,'hello aaron','Lance Lance','bdf04b26-b7a5-15bb-bd1c-a7c4888e2e9f',1472740691),(6,'sampleulit','Aaron Angeles','6e35590f-0229-b858-c1a6-72f1b7c3c243',1472741160),(7,'sample din','Lance Lance','9ddc0034-e862-c4e6-ffcf-cd3f26a21ee6',1472741174),(8,'hello','Aaron Angeles','66169c1d-b0db-d88b-9dd4-c540689942e2',1472742418),(9,'sample ulit','Lance Lance','4b7f230d-5b5a-a8d9-d436-d0b29854d66b',1472742445),(10,'plokplok','Lance Lance','639965ed-1ade-f0fa-fecd-fc7647c7ba46',1472805800),(11,'plokplok din','Aaron Angeles','7a6c3a2d-09d8-b73e-19d0-8d9370a9cffa',1472805881),(12,'sample','Aaron Neil Angeles','0e94307c-2fbf-3d12-35f1-96fd3b88063a',1472829363);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_article`
--

DROP TABLE IF EXISTS `tbl_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_article` (
  `ArticleID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `Content` longtext,
  `Author` varchar(45) DEFAULT NULL,
  `Date_posted` date DEFAULT NULL,
  `CatID` varchar(45) DEFAULT NULL,
  `UserID` varchar(45) DEFAULT NULL,
  `ViewCounter` int(11) DEFAULT '0',
  PRIMARY KEY (`ArticleID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_article`
--

LOCK TABLES `tbl_article` WRITE;
/*!40000 ALTER TABLE `tbl_article` DISABLE KEYS */;
INSERT INTO `tbl_article` VALUES (1,'SAMPLE ARTICLE','<p>\r\n	<span style=\"font-size:24px;\"><span style=\"font-family:comic sans ms,cursive;\"><strong>SAMPLE ARTICLE</strong></span></span></p>\r\n','Aaron Neil Angeles','2016-08-06','5','1',3),(2,'SAMPLE 2','<p>\r\n	SAMPLE2</p>\r\n','Bennedict Alboleras','2016-08-05','3','3',1),(4,'SAMPLE 4','<p>\r\n	SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4SAMPLE 4</p>\r\n','Aaron Neil Angeles','2016-08-06','3','1',3),(5,'SAMPLE 5','<p>\r\n	SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5SAMPLE 5</p>\r\n','Aaron Neil Angeles','2016-08-06','3','1',32),(6,'SAMPLE 6','<p>\r\n	SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6SAMPLE 6</p>\r\n','Aaron Neil Angeles','2016-08-06','8','1',49);
/*!40000 ALTER TABLE `tbl_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_article_category`
--

DROP TABLE IF EXISTS `tbl_article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_article_category` (
  `CatID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `StatusID` varchar(45) DEFAULT '1',
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_article_category`
--

LOCK TABLES `tbl_article_category` WRITE;
/*!40000 ALTER TABLE `tbl_article_category` DISABLE KEYS */;
INSERT INTO `tbl_article_category` VALUES (1,'Memorandum','1'),(2,'Circular','1'),(3,'Executive Order','1'),(4,'Proclamation','1'),(5,'Republic Act','1'),(6,'Appointments and Designation','1'),(7,'Others','1'),(8,'House Bill','1');
/*!40000 ALTER TABLE `tbl_article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_comments`
--

DROP TABLE IF EXISTS `tbl_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_comments` (
  `CommentID` int(11) NOT NULL AUTO_INCREMENT,
  `ThreadID` int(11) DEFAULT NULL,
  `UserID` varchar(45) DEFAULT NULL,
  `Comment` longtext,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`CommentID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_comments`
--

LOCK TABLES `tbl_comments` WRITE;
/*!40000 ALTER TABLE `tbl_comments` DISABLE KEYS */;
INSERT INTO `tbl_comments` VALUES (1,5,'1','sample sample','2016-08-25');
/*!40000 ALTER TABLE `tbl_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_front_thread`
--

DROP TABLE IF EXISTS `tbl_front_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_front_thread` (
  `ThreadID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(45) DEFAULT NULL,
  `UserID` varchar(45) DEFAULT NULL,
  `Content` longtext,
  `Views` int(11) DEFAULT '1',
  `LastPostBy` varchar(45) DEFAULT NULL,
  `LastPostTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ThreadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_front_thread`
--

LOCK TABLES `tbl_front_thread` WRITE;
/*!40000 ALTER TABLE `tbl_front_thread` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_front_thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_front_user`
--

DROP TABLE IF EXISTS `tbl_front_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_front_user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `fb_id` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `QuestionID` int(11) DEFAULT NULL,
  `QuestionAnswer` varchar(255) DEFAULT NULL,
  `fb_fname` varchar(45) DEFAULT NULL,
  `fb_lname` varchar(45) DEFAULT NULL,
  `fb_picture` text,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_front_user`
--

LOCK TABLES `tbl_front_user` WRITE;
/*!40000 ALTER TABLE `tbl_front_user` DISABLE KEYS */;
INSERT INTO `tbl_front_user` VALUES (1,'Aaron Neil','Angeles','1399834296697721','selegna_8@yahoo.com.ph','aaronangeles','aaronangeles',3,'blue','Aaron','Angeles','{\"data\":{\"is_silhouette\":false,\"url\":\"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p50x50/12661852_1246036188744200_8447413050747925823_n.jpg?oh=5b122c9c4371f086a0185244b43bdcba&oe=585476D0&__gda__=1480867914_d492db7d342407f9cab9c972b880ba31\"}}'),(2,'Lance','Lance','1022345867882548','iamselegna@gmail.com',NULL,NULL,NULL,NULL,'Lance','Lance','{\"data\":{\"is_silhouette\":false,\"url\":\"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p50x50/1507624_739555516161586_3598834701600675658_n.jpg?oh=2f810c4ec0f12f406d3e2190d1d725bb&oe=5851753E&__gda__=1480395819_c782c2eb2810a8b00a6e720b7637722a\"}}'),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_front_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sec_question`
--

DROP TABLE IF EXISTS `tbl_sec_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sec_question` (
  `QuestionID` int(11) NOT NULL,
  `Details` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sec_question`
--

LOCK TABLES `tbl_sec_question` WRITE;
/*!40000 ALTER TABLE `tbl_sec_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sec_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status`
--

DROP TABLE IF EXISTS `tbl_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status` (
  `StatusID` int(1) NOT NULL AUTO_INCREMENT,
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status`
--

LOCK TABLES `tbl_status` WRITE;
/*!40000 ALTER TABLE `tbl_status` DISABLE KEYS */;
INSERT INTO `tbl_status` VALUES (1,'Active'),(2,'Inactive');
/*!40000 ALTER TABLE `tbl_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_type`
--

DROP TABLE IF EXISTS `tbl_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_type` (
  `TypeID` int(1) NOT NULL AUTO_INCREMENT,
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_type`
--

LOCK TABLES `tbl_type` WRITE;
/*!40000 ALTER TABLE `tbl_type` DISABLE KEYS */;
INSERT INTO `tbl_type` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `tbl_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `Mname` varchar(45) DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Department` varchar(45) DEFAULT NULL,
  `StatusID` varchar(45) DEFAULT '1',
  `Email` varchar(45) DEFAULT NULL,
  `TypeID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'admin','7b902e6ff1db9f560443f2048974fd7d386975b0','Aaron Neil','Oximina','Angeles','Legal','1','angelesa@gmail.com','1'),(3,'user','d7316a3074d562269cf4302e4eed46369b523687','Bennedict','Lopez','Alboleras','Financing','1','dikkyalboleras@gmail.com','2');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-10 18:13:14
